<?php

/*-------------------------------
	Admin Page Settings Page
	https://codex.wordpress.org/Creating_Options_Pages
---------------------------------*/
class KLOC_Ticket_Manager_Settings_Page
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_styles' ));
    }

	// scripts_and_styles
	public function scripts_and_styles(){
		wp_enqueue_script( 'woocommerce-ticket-jquery', plugin_dir_url( __FILE__ ).'/lib/jquery-ui/jquery-ui.js', array( 'jquery' ) );
		wp_enqueue_script( 'woocommerce-ticket-jqueryui', plugin_dir_url( __FILE__ ).'/lib/jquery-ui/jquery-ui.js', array( 'jquery' ) );
		wp_enqueue_style( 'woocommerce-ticket-styles', plugin_dir_url( __FILE__ ).'/styles/style.css' );
		wp_enqueue_style( 'woocommerce-ticket-styles2', plugin_dir_url( __FILE__ ).'/lib/jquery-ui/jquery-ui.css' );
	}
	
    /**
     * Add options page
     */
    public function add_plugin_page()
    {
		  
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Group Ticket Settings', 
            'manage_options', 
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'kloc_ticket_manager' );
		
        ?>
        <div class="wrap">
            <h2>Ticket Management</h2>    

			 <h2 class="nav-tab-wrapper">
				<a href="#" class="nav-tab">Settings</a>
				
			</h2>
			
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields('my_option_group');   
				
				// For this page
                do_settings_sections( 'my-setting-admin' );
				
						
                submit_button(); 
            ?>
            </form>
			<script>
			// Initialise datepicker
			jQuery(function($){
				$(".datepicker" ).datepicker({ 
					dateFormat: 'dd/mm/yy',
					showOtherMonths: true,
					dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
					firstDay: 1
				});
				
			});
				
		</script>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'kloc_ticket_manager', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

		/*--------------------------
			Settings Section
		----------------------------*/
        add_settings_section(
            'group_ticket_settings', // ID
            'General Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );  

		add_settings_section(
            'group_ticket_discounts', // ID
            'Discount Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );  
		
		/* Date Settings */
		   add_settings_section(
            'group_ticket_dates', // ID
            'Park Season Dates', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        ); 
 
		/*--------------------------
			Settings Fields
		----------------------------*/
		
		/* Extra Fee */
		add_settings_field(
            'group_ticket_extrafee_product', 
            'Group Ticket Extra Fee Product SKU', 
            array( $this, 'group_ticket_extrafee_product_callback' ), 
            'my-setting-admin', 
            'group_ticket_discounts'
        );  
			
 		add_settings_field(
            'group_ticket_extrafee_product_option', 
            'Group Ticket Extra Fee Product Option', 
            array( $this, 'group_ticket_extrafee_product_option_callback' ), 
            'my-setting-admin', 
            'group_ticket_discounts'
        );  
		
 		add_settings_field(
            'group_ticket_extrafee_amount', 
            'Group Ticket Extra Fee Amount', 
            array( $this, 'group_ticket_extrafee_amount_callback' ), 
            'my-setting-admin', 
            'group_ticket_discounts'
        );  
		
		add_settings_field(
            'group_ticket_categories', 
            'Group Ticket Categories', 
            array( $this, 'group_ticket_categories_callback' ), 
            'my-setting-admin', 
            'group_ticket_settings'
        );  
		
		
		/* Group Ticket Numbers */
		add_settings_field(
            'group_ticket_prefix', 
            'Group Ticket Prefix', 
            array( $this, 'group_ticket_prefix_callback' ), 
            'my-setting-admin', 
            'group_ticket_settings'
        );  
		
		add_settings_field(
            'group_ticket_number', 
            'Last Group Ticket Number', 
            array( $this, 'group_ticket_number_callback' ), 
            'my-setting-admin', 
            'group_ticket_settings'
        );  
	
	
		 /* Group Ticket SKUS */
		add_settings_field(
            'group_ticket_skus', 
            'Group Ticket SKUs', 
            array( $this, 'group_ticket_skus_callback' ), 
            'my-setting-admin', 
            'group_ticket_settings'
        );  
		 				
		/* Low Season Start / End Ranges */
		 add_settings_field(
            'lowseason_daterange', 
            'Low Season', 
            array( $this, 'lowseason_daterange_callback' ), 
            'my-setting-admin', 
            'group_ticket_dates'
        );    
 		
		/* High Season Start / End Ranges */
		 add_settings_field(
            'highseason_daterange', 
            'High Season', 
            array( $this, 'highseason_daterange_callback' ), 
            'my-setting-admin', 
            'group_ticket_dates'
        );     

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

		/*
		if( get_option("kloc_ticket_manager") === false ) {
			add_option("kloc_ticket_manager", $new_input);
		}
		else {
			// holds : array( 'cat' => $display_category, 'product' => $single_post_ID );
			$my_param = get_option("_arr_params");
		}
		*/	 
		
		if( isset( $input['group_ticket_extrafee_product'] ) )
            $new_input['group_ticket_extrafee_product'] = sanitize_text_field( $input['group_ticket_extrafee_product'] );
	
		if( isset( $input['group_ticket_categories'] ) )
			$new_input['group_ticket_categories'] = sanitize_text_field( $input['group_ticket_categories'] );
		
		
		if( isset( $input['group_ticket_extrafee_product_option'] ) )
            $new_input['group_ticket_extrafee_product_option'] = sanitize_text_field( $input['group_ticket_extrafee_product_option'] );
		
		if( isset( $input['group_ticket_extrafee_amount'] ) )
            $new_input['group_ticket_extrafee_amount'] = sanitize_text_field( $input['group_ticket_extrafee_amount'] );
		
		if( isset( $input['group_ticket_prefix'] ) )
            $new_input['group_ticket_prefix'] = sanitize_text_field( $input['group_ticket_prefix'] );

		if( isset( $input['group_ticket_skus'] ) )
            $new_input['group_ticket_skus'] = sanitize_text_field( $input['group_ticket_skus'] );
		

		if( isset( $input['group_ticket_number'] ) )
            $new_input['group_ticket_number'] = absint( $input['group_ticket_number'] );
	
		if( isset( $input['highseason_daterange']) )
		{
			$new_input['highseason_daterange'] =  $input['highseason_daterange'];		
		}
		
		if( isset( $input['lowseason_daterange']) )
		{
			$new_input['lowseason_daterange'] =  $input['lowseason_daterange'];		
		}		
		
        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
       // print 'Enter your settings below:';
    }

	
	/** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_categories_callback()
    {
        printf(
            '<input type="text" id="group_ticket_categories" name="kloc_ticket_manager[group_ticket_categories]" value="%s" />',
            isset( $this->options['group_ticket_categories'] ) ? esc_attr( $this->options['group_ticket_categories']) : ''
        );
    }
	
	 /** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_extrafee_product_callback()
    {
        printf(
            '<input type="text" id="group_ticket_extrafee_product" name="kloc_ticket_manager[group_ticket_extrafee_product]" value="%s" />',
            isset( $this->options['group_ticket_extrafee_product'] ) ? esc_attr( $this->options['group_ticket_extrafee_product']) : ''
        );
    }
	
	 /** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_extrafee_product_option_callback()
    {
        printf(
            '<input type="text" id="group_ticket_extrafee_product_option" name="kloc_ticket_manager[group_ticket_extrafee_product_option]" value="%s" />',
            isset( $this->options['group_ticket_extrafee_product_option'] ) ? esc_attr( $this->options['group_ticket_extrafee_product_option']) : ''
        );
    }
	
		 /** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_skus_callback()
    {
        printf(
            '<input type="text" id="group_ticket_skus" name="kloc_ticket_manager[group_ticket_skus]" value="%s" />',
            isset( $this->options['group_ticket_skus'] ) ? esc_attr( $this->options['group_ticket_skus']) : ''
        );
    }
	
	/** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_extrafee_amount_callback()
    {
        printf(
            '<input type="text" id="group_ticket_extrafee_amount" name="kloc_ticket_manager[group_ticket_extrafee_amount]" value="%s" />',
            isset( $this->options['group_ticket_extrafee_amount'] ) ? esc_attr( $this->options['group_ticket_extrafee_amount']) : ''
        );
    }
	
	 /** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_prefix_callback()
    {
        printf(
            '<input type="text" id="group_ticket_prefix" name="kloc_ticket_manager[group_ticket_prefix]" value="%s" />',
            isset( $this->options['group_ticket_prefix'] ) ? esc_attr( $this->options['group_ticket_prefix']) : ''
        );
    }
	
	 /** 
     * Get the settings option array and print one of its values
     */
    public function group_ticket_number_callback()
    {
        printf(
            '<input type="text" id="group_ticket_number" name="kloc_ticket_manager[group_ticket_number]" value="%s" />',
            isset( $this->options['group_ticket_number'] ) ? esc_attr( $this->options['group_ticket_number']) : 6000
        );
    }
	

	/** 
     * Get the settings option array and print one of its values
     */
    public function highseason_daterange_callback()
    {
		print "<p>Please enter dates in YYYY-MM-DD format</p>";
		
		printf(
            '<label for="highseason_daterange">Start</label><input type="text" class="datepicker" id="highseason_daterange" name="kloc_ticket_manager[highseason_daterange][0][start]" value="%s" />',
            isset($this->options['highseason_daterange'][0]["start"]) ? esc_attr( $this->options['highseason_daterange'][0]["start"]) : ''
        );
		
		 printf(
            '<label for="highseason_daterange2">End</label><input type="text" class="datepicker" id="highseason_daterange2" name="kloc_ticket_manager[highseason_daterange][0][end]" value="%s" />',
            isset( $this->options['highseason_daterange'][0]["end"]) ? esc_attr( $this->options['highseason_daterange'][0]["end"]) : ''
        );
		
		printf(
            '<label for="highseason_daterange3">Start</label><input type="text" class="datepicker" id="highseason_daterange3" name="kloc_ticket_manager[highseason_daterange][1][start]" value="%s" />',
            isset($this->options['highseason_daterange'][1]["start"]) ? esc_attr( $this->options['highseason_daterange'][1]["start"]) : ''
        );
		
		 printf(
            '<label for="highseason_daterange4">End</label><input type="text" class="datepicker" id="highseason_daterange4" name="kloc_ticket_manager[highseason_daterange][1][end]" value="%s" />',
            isset( $this->options['highseason_daterange'][1]["end"]) ? esc_attr( $this->options['highseason_daterange'][1]["end"]) : ''
        );
    }
	
	/** 
     * Get the settings option array and print one of its values
     */
    public function lowseason_daterange_callback()
    {
		print "<p>Please enter dates in YYYY-MM-DD format</p>";
      
		printf(
            '<label for="lowseason_daterange">Start</label><input type="text" class="datepicker" id="lowseason_daterange" name="kloc_ticket_manager[lowseason_daterange][0][start]" value="%s" />',
            isset($this->options['lowseason_daterange'][0]["start"]) ? esc_attr( $this->options['lowseason_daterange'][0]["start"]) : ''
        );
		
		 printf(
            '<label for="lowseason_daterange2">End</label><input type="text" class="datepicker" id="lowseason_daterange2" name="kloc_ticket_manager[lowseason_daterange][0][end]" value="%s" />',
            isset( $this->options['lowseason_daterange'][0]["end"]) ? esc_attr( $this->options['lowseason_daterange'][0]["end"]) : ''
        );
		
		printf(
            '<label for="lowseason_daterange3">Start</label><input type="text" class="datepicker" id="lowseason_daterange3" name="kloc_ticket_manager[lowseason_daterange][1][start]" value="%s" />',
            isset($this->options['lowseason_daterange'][1]["start"]) ? esc_attr( $this->options['lowseason_daterange'][1]["start"]) : ''
        );
		
		 printf(
            '<label for="lowseason_daterange4">End</label><input type="text" class="datepicker" id="lowseason_daterange4" name="kloc_ticket_manager[lowseason_daterange][1][end]" value="%s" />',
            isset( $this->options['lowseason_daterange'][1]["end"]) ? esc_attr( $this->options['lowseason_daterange'][1]["end"]) : ''
        );
    }
		
}

if( is_admin() )
    $my_settings_page = new KLOC_Ticket_Manager_Settings_Page();
?>