<?php
  /*http://www.measureddesigns.com/woocommerce-adding-company-name-to-order-email/*/
/*
function kloc_ticket_manager_woocommerce_locate_template( $template, $template_name, $template_path ) {
 
   global $woocommerce;

  
  if ( ! $template_path ) $template_path = $woocommerce->template_url;
  
  $plugin_path  = kloc_ticket_manager_plugin_path() . '/woocommerce/';
 
  // Look within passed path within the theme - this is priority
  $template = locate_template(
 
    array(
      $template_path . $template_name,
      $template_name
    )
  );
  

  // Modification: Get the template from this plugin, if it exists 
  if ( ! $template && file_exists( $plugin_path . $template_name ) )
    $template = $plugin_path . $template_name;
 
 
  // Use default template
  if ( ! $template )
    $template = $_template;

}
 */
 
 /*---------------------------
	Add Capability to manage group tickets
 ----------------------------*/
function add_manage_group_tickets_capability() 
{
 //   $role = get_role('shop manager');
 //   $role->add_capability('access_manage_group_tickets' );
}

add_action( 'admin_init', 'add_manage_group_tickets_capability');
 
/*-------------------------------------------------
	Admin Shop Order Screen
	http://stackoverflow.com/questions/13683162/woocommerce-show-custom-column
	
------------------------------------------------*/
add_filter( 'manage_edit-shop_order_columns', 'kloc_ticket_manager_admin_order_columns' );

function kloc_ticket_manager_admin_order_columns($columns){
    $new_columns = (is_array($columns)) ? $columns : array();
    unset( $new_columns['order_title'] );
	unset( $new_columns['order_status'] );
	unset( $new_columns['order_items'] );
	unset( $new_columns['billing_address'] );
	unset( $new_columns['shipping_address'] );
	unset( $new_columns['customer_message'] );
	unset( $new_columns['order_notes'] );
	unset( $new_columns['order_date'] );
	unset( $new_columns['order_total'] );
	unset( $new_columns['order_actions'] );

    $new_columns['order_title'] = $columns['order_title'];
	$new_columns['order_status'] =$columns['order_status'];
	$new_columns['order_date'] =$columns['order_date'];	
		
	//edit this for you column(s)
    //all of your columns will be added before the actions column
    $new_columns['contains_group_ticket_id'] = 'Contains Group Ticket';
    $new_columns['group_ticket_number_id'] = 'Group Ticket Number';
    //stop editing
	

	$new_columns['order_items']=$columns['order_items'];
	$new_columns['billing_address'] =$columns['billing_address'];
	$new_columns['shipping_address'] =$columns['shipping_address'];
	$new_columns['customer_message'] =$columns['customer_message'];
	$new_columns['order_notes']=$columns['order_notes'];

	$new_columns['order_total'] =$columns['order_total'];
	$new_columns['order_actions'] =$columns['order_actions'];
	
    return $new_columns;
}
 
add_action( 'manage_shop_order_posts_custom_column', 'kloc_ticket_manager_admin_order_columns_manage', 2 );
function kloc_ticket_manager_admin_order_columns_manage($column){
    global $post;
    $data = get_post_meta( $post->ID );

    //start editing, I was saving my fields for the orders as custom post meta
    //if you did the same, follow this code
    if ( $column == 'contains_group_ticket_id' ) {    
        echo (isset($data['Group Ticket Number']) ? 'yes' : '');
    }
    if ( $column == 'group_ticket_number_id' ) {  
        echo (isset($data['Group Ticket Number']) ? $data['Group Ticket Number'][0] : '');
    }
    //stop editing
}
 
add_filter( "manage_edit-shop_order_sortable_columns", 'kloc_ticket_manager_admin_order_columns_sort' );

function kloc_ticket_manager_admin_order_columns_sort( $columns ) {
    $custom = array(
        //start editing

        'contains_group_ticket_id'    => 'Group Ticket Number',
        'group_ticket_number_id'    => 'Group Ticket Number'

        //stop editing
    );
    return wp_parse_args( $custom, $columns );
}
 
 
/*---------------------------------------------------------------------------------------
	Locate Template 
	https://www.skyverge.com/blog/override-woocommerce-template-file-within-a-plugin/
-------------------------------------------------------------------------------------------*/

function kloc_ticket_manager_plugin_path() {
 
  // gets the absolute path to this plugin directory
  return untrailingslashit( plugin_dir_path( __FILE__ ) );
}

add_filter('woocommerce_locate_template', 'kloc_ticket_manager_woocommerce_locate_template', 10, 3 );
add_filter('woocommerce_get_template', 'kloc_ticket_manager_woocommerce_locate_template', 10, 3 );

function kloc_ticket_manager_woocommerce_locate_template($template, $template_name, $template_path) {
	
	//print($template.", --->". $template_name.",    -->".$template_path);
	//die;
    if ($template_name == 'emails\admin-grouptickets-order.php') {
		
		$group_ticket_path =  kloc_ticket_manager_plugin_path()."\woocommerce\\emails\\admin-grouptickets-order.php";
        $template = $str = str_replace('/', '\\', $group_ticket_path);
	}
	if ($template_name == 'emails\plain\admin-grouptickets-order.php') {
		
		$group_ticket_path =  kloc_ticket_manager_plugin_path()."\woocommerce\\emails\\plain\\admin-grouptickets-order.php";
        $template = $str = str_replace('/', '\\', $group_ticket_path);
	}
		
    return $template;
}

/*-------------------------------------------------------------------
	Extend Woocommerce Email Functionality
	Add a custom email to the list of emails WooCommerce should load
-------------------------------------------------------------------*/

function add_groupticket_order_woocommerce_email( $email_classes ) {

	// include our custom email class
	require_once( 'includes/emails/class-wc-groupticket-order-email.php' );

	// add the email class to the list of email classes that WooCommerce loads
	$email_classes['WC_GroupTicket_Order_Email'] = new WC_GroupTicket_Order_Email(kloc_ticket_manager_plugin_path());

	return $email_classes;

}

add_filter( 'woocommerce_email_classes', 'add_groupticket_order_woocommerce_email' );


/*---------------------------------------------------------
	Add Filter Order By Products functionality
------------------------------------------------------------*/

require_once( 'includes/order-filter/woocommerce-filter-orders-by-product.php' );
FOA_Woo_Filter_Orders_by_Product::instance();

//require_once( 'includes/order-filter/woocommerce-filter-orders-by-category.php' );
//FOA_Woo_Filter_Orders_by_Category::instance();

/*------------------------------------------------------------
 Redirect if there is only one product in the category 
--------------------------------------------------------------*/

add_action( 'wp_head', 'kloc_ticket_manager_redirect_if_single_product_in_category', 10 );

	function kloc_ticket_manager_redirect_if_single_product_in_category() 
	{
		global $wp_query;
		global $ticket_helper;  // KLOC ticket helper....
		
			$product = new WC_Product($wp_query->post->ID);
			$group_tickets_affected = $ticket_helper->getGroupTicketProductData();	
			
			if($product)
			{
				// And product matches the ones we are on the lookout for then redirect to the other page!!
				//if ($product->is_visible())
				//{		
					if(in_array(strtolower($product->sku), $group_tickets_affected["product_sku"]))
					{					
						// Now only do this if the product sku is within the types we are checking for
						$link = get_permalink($product->post->id);
						echo "<meta http-equiv='refresh' content='0;url=group-tickets/' />";
						//wp_redirect("/group-ticket/", 302 );
						exit;
					}
				//} // if can show
				
			} // if $product exists

	return false;
}

/*-------------------------------------------------------------------
Add extra �10 fee when a specific product / option is chosen
--------------------------------------------------------------------*/
/*
add_action('woocommerce_add_to_cart', 'kloc_ticket_manager_woocommerce_add_to_cart', 10, 6);

function kloc_ticket_manager_woocommerce_add_to_cart( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ){
    
	$incentive_product_id = 506;
    $category = 'fee';
    $taxonomy = 'fee';

    if( has_term( $category, $taxonomy, $product_id ) ){    
        $cart_id = WC()->cart->generate_cart_id( $incentive_product_id );
        $prod_in_cart = WC()->cart->find_product_in_cart( $cart_id );

        // Add the product only if it's not in the cart already
        if( ! $prod_in_cart ) {
            WC()->cart->add_to_cart( $incentive_product_id );
        }
    }
}*/

/*--------------------------------------------------------------------
Add extra �10 fee when a specific product / option is chosen
---------------------------------------------------------------------*/

function kloc_ticket_manager_add_ticket_surcharge( $cart_object ) {

    global $woocommerce;
	global $ticket_helper;  // KLOC ticket helper....
	
	$surchage_options = $ticket_helper->getGroupTicketSurcharge();
	
    foreach ( $cart_object->cart_contents as $key => $value ) 
	{
        $proid = $value['product_id']; //get the product id from cart
        $quantiy = $value['quantity']; //get quantity from cart
        $itmprice = $value['data']->price; //get product price
		
		$_pf = new WC_Product_Factory();  
		$_product = $_pf->get_product($proid);
		
		$product_addons = get_product_addons( $proid );
		
		// Check the SKU exists and then that the addons exist
		if(in_array(strtolower($_product->sku), $surchage_options["product_sku"]))
		{	
			if(isset($surchage_options["product_option"]))
			{
				// Has a value to check against
				if(count($surchage_options["product_option"]) > 0)
				{
					if(array_key_exists("addons", $value))
					{
						foreach ($value["addons"] as $addonkey => $addonvalue ) 
						{
							// Take the array and make it  a ", separated, string"
							// does the current product addon name exist in the ones we should be looking for?
							// case insensitive...
							$surcharge_str = strtolower(implode($surchage_options["product_option"]));
							//if(stristr($addonvalue["name"], ) >0)

							// Horrible string in string match
							if(strpos(strtolower($addonvalue["name"]), $surcharge_str) !== false)
							{
								$woocommerce->cart->add_fee('Group Ticket Admin Fee', $surchage_options["amount"], true, 'standard' );				
							}
						}
					}
				}				
			}
		}
    }    
}

add_action( 'woocommerce_cart_calculate_fees', 'kloc_ticket_manager_add_ticket_surcharge' );


/*-----------------------------------------------------
*	Admin order edit screen
------------------------------------------------------*/
add_action( 'woocommerce_before_order_meta', 'kloc_ticket_manager_before_order_itemmeta', 10, 3 );
function kloc_ticket_manager_before_order_itemmeta( $item_id, $item, $_product ){
 
}

/*-----------------------------------------------------
 * Display field value on the order edit page
 -----------------------------------------------------*/
add_action( 'woocommerce_admin_order_data_after_order_details', 'kloc_ticket_manager_display_admin_order_meta', 10, 1 );

function kloc_ticket_manager_display_admin_order_meta($order){
	
    echo '<br /><br /><p><strong>'.__('Group Ticket Number').':</strong> <br/>' . get_post_meta( $order->id, 'Group Ticket Number', true ) . '</p>';
}

/*----------------------------------------------------
*	Hook into add to cart to save extra order item meta
https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
------------------------------------------------------*/

add_action( 'woocommerce_checkout_update_order_meta', 'kloc_ticket_manager_order_meta', 10, 2 );
 
function kloc_ticket_manager_order_meta($order_id, $posted) {
	
    global $woocommerce;
	global $ticket_helper;  // KLOC ticket helper....
	
	$group_tickets_affected = $ticket_helper->getGroupTicketProductData();	

	$order = new WC_Order( $order_id );
	$items = $order->get_items();
	
	foreach ($items as $item) 
	{
	  $product_id = $item['product_id'];
	  $product_name = $item['name'];
  
		$_pf = new WC_Product_Factory();  
		$_product = $_pf->get_product($product_id);
			
		if(in_array(strtolower($_product->sku), $group_tickets_affected["product_sku"]))
		{	
			// If this order contains a product that is of type ticket then lets add a ticket number
			//$item_meta->add('Group Ticket Number', $ticket_helper->getNextGroupTicketNumber());
			update_post_meta($order_id, 'Group Ticket Number', $ticket_helper->getNextGroupTicketNumber());

		}
	}
	
	// Save other meta data we have added
	// if ( ! empty( $_POST['my_field_name'] ) ) {
    //    update_post_meta( $order_id, 'My Field', sanitize_text_field( $_POST['my_field_name'] ) );
   // }
	
}

/*----------------------------------------------------
	Hook into add to cart validation
------------------------------------------------------*/
add_filter( 'woocommerce_add_to_cart_validation', 'kloc_ticket_manager_validate_add_cart_item', 10, 5 );

function kloc_ticket_manager_validate_add_cart_item( $passed, $product_id, $quantity, $variation_id = '', $variations= '' ) {

// do your validation, if not met switch $passed to false
	 global $woocommerce;
	global $ticket_helper;  // KLOC ticket helper....

		$_pf = new WC_Product_Factory();  
		$product = $_pf->get_product($product_id);
		
		$group_tickets_affected = $ticket_helper->getGroupTicketProductData();	
		
		if($product)
		{			
			// And product matches the ones we are on the lookout for then redirect to the other page!!
			if ($product->is_visible())
			{		
	
				if(in_array(strtolower($product->sku), $group_tickets_affected["product_sku"]))
				{	
					// This is the product in question.
					
					// Check we have a date set?
					if(!$ticket_helper->hasBeenSet($product_id, "date-time-of-visit"))
					{
						$passed = false;
						wc_add_notice( __( 'Please ensure a date / time is specified', 'textdomain' ), 'error' );
					}
					
					// Check the numbers
					$totals = $ticket_helper->getNumbersInGroup();	
				
					if ($totals < $ticket_helper->getGroupTicketMinimumNumbers())
					{
						$passed = false;
						wc_add_notice( __( 'Please ensure '.$ticket_helper->getGroupTicketMinimumNumbers().' or more items are added', 'textdomain' ), 'error' );
					}
				}			
			}
		}
			
	return $passed;
}

/*-------------------------------------
Add extra fields on checkout 
These should save automatically..
-------------------------------------*/
// Hook in
add_filter( 'woocommerce_checkout_fields' , 'kloc_ticket_manager_custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function kloc_ticket_manager_custom_override_checkout_fields( $fields ) 
{
	// Add field for shipping organisation name 
     $fields['shipping']['shipping_organisation'] = array(
        'label'     => __('Organisation Name', 'woocommerce'),
    'placeholder'   => _x('Organisation Name', 'placeholder', 'woocommerce'),
    'required'  => false,
    'class'     => array('form-row-wide'),
    'clear'     => true
     );
	 
	 $shipping_order = array(
        "shipping_first_name", 
        "shipping_last_name", 
		//"shipping_email",
		//"shipping_phone",
		"shipping_country",
        "shipping_company",
		"shipping_organisation",
        "shipping_address_1", 
        "shipping_address_2", 
		"shipping_city",
		"shipping_state",
		"shipping_postcode"
    );
    foreach($shipping_order as $shipping_order_field)
    {
        $shipping_ordered_fields[$shipping_order_field] = $fields["shipping"][$shipping_order_field];
    }

    $fields["shipping"] = $shipping_ordered_fields;
	 
	 

	 // Add field for billing organisation name
	  $fields['billing']['billing_organisation'] = array(
       'label'     => __('Organisation Name', 'woocommerce'),
		'placeholder'   => _x('Organisation Name', 'placeholder', 'woocommerce'),
		'required'  => false,
		'class'     => array('form-row-wide'),
		'clear'     => true
		 );

    $billing_order = array(
        "billing_first_name", 
        "billing_last_name", 
		"billing_email",
		"billing_phone",
		"billing_country",
        "billing_company",
		"billing_organisation",
        "billing_address_1", 
        "billing_address_2", 
		"billing_city",
		"billing_state",
		"billing_postcode"
    );
    foreach($billing_order as $billing_order_field)
    {
        $billing_ordered_fields[$billing_order_field] = $fields["billing"][$billing_order_field];
    }

    $fields["billing"] = $billing_ordered_fields;
	
    return $fields;
}

/*-------------------------------------
Cart item name
-------------------------------------
add_filter( 'woocommerce_cart_item_name', 'kloc_ticket_manager_cart_product_title', 20, 3);

function kloc_ticket_manager_cart_product_title( $title, $values, $cart_item_key ) {
    return $title . ' - ' . $values[ 'line_total' ] . '';
}*/


/*----------------------------------------------------
*	Has this product already been added to the cart??
------------------------------------------------------*/
function kloc_ticket_manager_check_if_cart_has_product( $valid, $product_id, $quantity) 
{  
	global $woocommerce;
	global $ticket_helper;  // KLOC ticket helper....
	
	$group_ticket_categories = $ticket_helper->getGroupTicketCategories();	
	$category_already_in_cart = false;
	
	// 1. Check that the product being added into the cart matches the group ticket categories.
		$can_restrict_existing_product = false;
		$_pf = new WC_Product_Factory();  
		$adding_product = $_pf->get_product($product_id);

		// Get categories of the product in the cart
		// Get product categories
		$adding_product_terms = get_the_terms( $product_id , 'product_cat' );

		foreach ($adding_product_terms as $adding_product_term) 
		{			
			// See if the term is in the list of terms we are looking for.
			if(in_array(strtolower($adding_product_term->name), $group_ticket_categories["group_ticket_categories"]))
			{	
				// We have a category match
				$can_restrict_existing_product = true;
				break;
			}
		}
	
	/// If we cant restrict existing product then lets just add it in to the cart, but if we should be restricting it lets
	// investigate further...

	if($can_restrict_existing_product)	
	{
		// 2. Check for existing cart product entries.
		// KLOC: Was causing issues locally....
		if(!empty(WC()->cart->get_cart()) && $valid)
		{
			foreach (WC()->cart->get_cart() as $cart_item_key => $values) 
			{
				// Lets look at the existing products in the cart.... 
				$_product = $values['data'];
				
				// Get the categories of this product id 
				$existing_product_terms = get_the_terms( $product_id , 'product_cat' );
				
				foreach ($existing_product_terms as $existing_product_term) 
				{
					// See if the term is in the list of terms we are looking for.
					if(in_array(strtolower($existing_product_term->name), $group_ticket_categories["group_ticket_categories"]))
					{	
						// We have a category match
						$category_already_in_cart = true;
						break;
					}
				}
				
				if($category_already_in_cart)
			    {
					wc_add_notice( 'Only one group ticket product can be added in the cart', 'error' );
					return false;
				}
			}
		}
	}
    return $valid;
}

add_filter( 'woocommerce_add_to_cart_validation', 'kloc_ticket_manager_check_if_cart_has_product', 10, 3 );

/*---------------------------------
	Group ticket orders page
----------------------------------*/

add_action('admin_menu', 'kloc_ticket_manager_group_ticket_orders_page', 10, 34);

function kloc_ticket_manager_group_ticket_orders_page() {

    add_submenu_page( 'woocommerce', 
	'Latest Group Ticket Orders', // page title
	'Group Ticket Orders',  // menu title
	'manage_options', // capability
	'manage-group-tickets', // menu slug
	'kloc_ticket_manager_group_ticket_orders_page_callback' ); // callback function
}

function kloc_ticket_manager_group_ticket_orders_page_callback()
{
    global $title;

    print '<div class="wrap">';
    print "<h1>$title</h1>";

    $file = plugin_dir_path( __FILE__ ) . "/includes/manage/group-ticket-order.php";

    if ( file_exists( $file ) )
        require $file;

	print "<br/>";

	// Hide orders that are not processing or completed
	$post_status = array( 'wc-processing', 'wc-completed' );

	// WP_Query arguments
	$args = array (
	'post_type'              => array( 'shop_order' ),
	'post_status' 			 => $post_status,
	'posts_per_page'		 => '100',
	'meta_query' => array(
	'relation' => 'AND',
	array(
		'key' => 'Group Ticket Number',
		'value' => 'c',
		'compare' => 'LIKE'
	)
	));

	print "<table class='wp-list-table widefat fixed striped posts'>";
	print "<thead>";
	print "<tr>";
	print "<th></th>";
	print "<th>Order Number</th>";
	print "<th>Group Ticket Number</th>";
	print "<th>Order Date</th>";
	print "<th>Order Status</th>";
	//print "<th>Visit Date</th>";
	print "<th>Organisation</th>";
	print "<th>Name</th>";
	print "<th>Phone</th>";
	print "<th>Email</th>";
	print "<th></th>";
	print "</tr>";
	print "</thead>";

	print "<tbody>";
	
	// The Query
	$query = new WP_Query($args);
	$count = 1; 
		// The Loop
		if ($query->have_posts())
		{
			while ($query->have_posts())
			{
				$query->the_post();
				$order = new WC_Order($query->post->ID);
				displayOrderHTML($order, $query->post->ID, $is_scansearch, $count);
				$count++;
			}
		}
		else
		{
			print "<tr>";
			print "<td colspan='8'>";
			print"	<p>"._e( 'Sorry, no group tickets matched your criteria.' )."</p>";
			print "</td>";
		}

		// Restore original Post Data
		wp_reset_postdata(); 	
		
		print "</tbody>";
		print "</table>";
		print "</div>";
	
}


/*--------------------------------------------
	Add attachments to email

add_filter( 'woocommerce_email_attachments', 'kloc_ticket_manager_add_files_to_email', 1, 3);

function kloc_ticket_manager_add_files_to_email( $attachments, $status, $order ) {

	$data = get_post_meta($order->id);

	if(isset($data['Group Ticket Number']))
	{
		$attachments[] = get_template_directory_uri()."/assets/group-ticket.pdf";
	}
	
	return $attachments;	
}
--------------------------------------------*/
?>