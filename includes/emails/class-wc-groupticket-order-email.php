<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A custom Group Ticket Order WooCommerce Email class
 *
 * @since 0.1
 * @extends \WC_Email
 */
class WC_GroupTicket_Order_Email extends WC_Email {


	/**
	 * Set email defaults
	 *
	 * @since 0.1
	 */
	public function __construct($plugin_dir_path) {
	
		//Set the calling plugin dir path
		$this->plugin_dir_path = $plugin_dir_path;
	
		// set ID, this simply needs to be a unique name
		$this->id = 'wc_groupticket_order';
		
		$this->customer_email = true;

		// this is the title in WooCommerce Email settings
		$this->title = 'Group Ticket Order';

		// this is the description in WooCommerce email settings
		$this->description = 'Sends an email when group tickets have been ordered';

		// these are the default heading and subject lines that can be overridden using the settings
		$this->heading = 'Thankyou for your order';
		$this->subject = 'Group Ticket Order';

		// these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
		
		$this->template_html  = 'emails/admin-grouptickets-order.php';
		$this->template_plain = 'emails/plain/admin-grouptickets-order.php';
		
		// Trigger on new paid orders
		add_action( 'woocommerce_order_status_pending_to_processing_notification', array( $this, 'trigger' ) );
		add_action( 'woocommerce_order_status_failed_to_processing_notification',  array( $this, 'trigger' ) );

		// Call parent constructor to load any other defaults not explicity defined here
		parent::__construct();
	}

	/**
	 * Determine if the email should actually be sent and setup email merge variables
	 *
	 * @since 0.1
	 * @param int $order_id
	 */
	public function trigger( $order_id ) {
		
		// this should be a comma separated string of categories
		$match_categories_str = $this->get_option('categories');
		
		$match_categories = explode(",", $match_categories_str);
		
		$can_send_email = false;
	
		// bail if no order ID is present
		if ( ! $order_id )
			return;

		// setup order object
		$this->object = new WC_Order( $order_id );

		// send email to customer and admin....
		$this->recipient = $this->object->billing_email.",".$this->get_option( 'recipient');
	
		// if none was entered, just use the WP admin email as a fallback
		if ( ! $this->recipient )
			$this->recipient = get_option( 'admin_email' );
			
		// Return if the order does not contain a group ticket!!.
        $items = $this->object->get_items(); 

		// Look at each item on the order
        foreach ( $items as $item ) 
		{
			// Get product id 
            $product_id = $item['product_id'];
			
			// Get product categories
			$terms = get_the_terms( $product_id , 'product_cat' );
			
			foreach ($terms as $term) 
			{
				// See if the term is in the list of terms we are looking for.
				if(in_array($term->name, $match_categories))
				{	
					// We have a category match
					$can_send_email = true;
					break;
				}
			}
        }
 
		if($can_send_email == false)
		{
			return;
		}
	
		//Otherwise the order has what we want - lets send a separate email.
 
		// replace variables in the subject/headings
		$this->find[] = '{order_date}';
		$this->replace[] = date_i18n( woocommerce_date_format(), strtotime( $this->object->order_date ) );

		$this->find[] = '{order_number}';
		$this->replace[] = $this->object->get_order_number();

		
		if ( ! $this->is_enabled() || ! $this->get_recipient() )
			return;

		// Attachments to be set on the edit email page.
		// Ideally make these manageable from the edit email page.
		$attachments = array();
		$attachments[] = get_template_directory_uri()."/assets/beale-park-map.pdf";
		$attachments[] = get_template_directory()."/assets/beale-park-map.pdf";
		$attachments[] = get_template_directory_uri()."/assets/letter-to-organisers.pdf";
		$attachments[] = get_template_directory()."/assets/letter-to-organisers.pdf";
		
		// woohoo, send the email!
		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $attachments);
	}


	/**
	 * get_content_html function.
	 *
	 * @since 0.1
	 * @return string
	 */
	public function get_content_html() {
	
		return wc_get_template_html( $this->template_html, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => false,
			'email'			=> $this
		) );
	}


	/**
	 * get_content_plain function.
	 *
	 * @since 0.1
	 * @return string
	 */
	function get_content_plain() {
		
		return wc_get_template_html( $this->template_plain, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => true,
			'email'			=> $this
		) );
	}	

	/**
	 * Initialize Settings Form Fields
	 *
	 * @since 2.0
	 */
	public function init_form_fields() {

		$attachments_default = get_template_directory_uri()."/assets/group-ticket.pdf";
	
		$this->form_fields = array(
			'enabled'    => array(
				'title'   => 'Enable/Disable',
				'type'    => 'checkbox',
				'label'   => 'Enable this email notification',
				'default' => 'yes'
			),
			'recipient'  => array(
				'title'       => 'Recipient(s)',
				'type'        => 'text',
				'description' => sprintf( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', esc_attr( get_option( 'admin_email' ) ) ),
				'placeholder' => '',
				'default'     => ''
			),
			'subject'    => array(
				'title'       => 'Subject',
				'type'        => 'text',
				'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
				'placeholder' => '',
				'default'     => ''
			),
			'attachments'    => array(
				'title'       => 'Attachments',
				'type'        => 'text',
				'description' => sprintf( __( 'This controls the attachments added to the email. Leave blank to use the default attachments: <code>%s</code>. - Default is '.$attachments_default ), $this->attachments ),
				'placeholder' => $attachments_default,
				'default'     => $attachments_default
			),
			'heading'    => array(
				'title'       => 'Email Heading',
				'type'        => 'text',
				'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
				'placeholder' => '',
				'default'     => ''
			),
			'categories'    => array(
				'title'       => 'Categories',
				'type'        => 'text',
				'description' => sprintf( __( 'Enter categories (comma separated) that this email will be sent for. ' ), $this->categories ),
				'placeholder' => '',
				'default'     => ''
			),
			'email_type' => array(
				'title'       => 'Email type',
				'type'        => 'select',
				'description' => 'Choose which format of email to send.',
				'default'     => 'html',
				'class'       => 'email_type',
				'options'     => array(
					'plain'	    => __( 'Plain text', 'woocommerce' ),
					'html' 	    => __( 'HTML', 'woocommerce' ),
					'multipart' => __( 'Multipart', 'woocommerce' ),
				)
			),
	/*
			'content_html_template' => array(
				'title'         => __( 'HTML template', 'woocommerce' ),
				'type'          => 'textarea',
				'description'   => '',
				'placeholder'   => '',
				'default'       => $this->get_content_html(),
				'css'           => 'width:66%;min-width:300px;height:500px;',
			),
			
			'content_plain_template' => array(
				'title'         => __( 'Plain text template', 'woocommerce' ),
				'type'          => 'textarea',
				'description'   => '',
				'placeholder'   => '',
				'default'       => $this->get_content_plain(),
				'css'           => 'width:66%;min-width:300px;height:500px;',
			),*/
		);
	}


} // end \WC_GroupTicket_Order_Email class
