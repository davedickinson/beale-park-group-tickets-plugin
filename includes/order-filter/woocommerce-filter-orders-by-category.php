<?php

if ( ! defined( 'WPINC' ) ) die;

class FOA_Woo_Filter_Orders_by_Category{
	private static $instance = null;

	private function __construct() {
		if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || !is_admin() ){
			return;
		}
		add_action( 'init', array( $this, 'load_textdomain' ) );
		add_action( 'restrict_manage_posts', array( $this, 'category_filter_in_order' ), 50  );
		add_action( 'posts_where', array( $this, 'category_filter_where' ));
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_styles' ));
	}

	public static function instance() {
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

    // Textdomain
    public function load_textdomain(){
        load_plugin_textdomain( 'woocommerce-filter-orders-by-category', false, dirname( plugin_basename(__FILE__) ) . '/languages/' );
    }

	// Display dropdown
	public function category_filter_in_order(){
		global $typenow, $wpdb;

		if ( 'shop_order' != $typenow ) {
			return;
		}

	    $sql="SELECT wp_terms.term_id, wp_terms.name
FROM wp_term_relationships
LEFT JOIN wp_term_taxonomy
   ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)
LEFT JOIN wp_terms on wp_term_taxonomy.term_taxonomy_id = wp_terms.term_id
WHERE wp_term_taxonomy.taxonomy = 'product_cat'
GROUP BY wp_term_taxonomy.term_id";

		$all_categories = $wpdb->get_results($sql, ARRAY_A);

		$values = array();

		foreach ($all_categories as $all_category) {
			$values[$all_category['name']] = $all_category['term_id'];
		}
	    ?>
	    <span id="foa_order_category_filter_wrap">
		    <select name="foa_order_category_filter" id="foa_order_category_filter">
		    <option value=""><?php _e('All categories', 'woocommerce-filter-orders-by-category'); ?></option>
		    <?php
		        $current_v = isset($_GET['foa_order_category_filter'])? $_GET['foa_order_category_filter']:'';
		        foreach ($values as $label => $value) {
		            printf
		                (
		                    '<option value="%s"%s>%s</option>',
		                    $value,
		                    $value == $current_v? ' selected="selected"':'',
		                    $label
		                );
		            }
		    ?>
		    </select>
		    <div id="fuzzSearch">
		    	<div id="fuzzNameContainer">
		    		<span class="fuzzName"></span>
		    		<span class="fuzzArrow"></span>
		    	</div>
		    	<div id="fuzzDropdownContainer">
		    		<input type="text" value="" class="fuzzMagicBox" placeholder="<?php _e('Search...', 'woocommerce-filter-orders-by-category'); ?>" />
		    		<ul id="fuzzResults">
		    		</ul>
		    	</div>
		    </div>
		</span>
	    <script type="text/javascript">
			jQuery('#foa_order_category_filter').fuzzyDropdown({
			  mainContainer: '#fuzzSearch',
			  arrowUpClass: 'fuzzArrowUp',
			  selectedClass: 'selected',
			  enableBrowserDefaultScroll: true
			});
	   </script>
	    <?php
	}

	// modify where clause in query
	public function category_filter_where( $where ) {
		if( is_search() ) {
			global $wpdb;
			$t_posts = $wpdb->posts;
			$t_order_items = $wpdb->prefix . "woocommerce_order_items";  
			$t_order_itemmeta = $wpdb->prefix . "woocommerce_order_itemmeta";

			if ( isset( $_GET['foa_order_category_filter'] ) && !empty( $_GET['foa_order_category_filter'] ) ) {
				$category = intval($_GET['foa_order_category_filter']);
				$where .= " AND $category IN (SELECT $t_order_itemmeta.meta_value FROM $t_order_items LEFT JOIN $t_order_itemmeta on $t_order_itemmeta.order_item_id=$t_order_items.order_item_id WHERE $t_order_items.order_item_type='line_item' AND $t_order_itemmeta.meta_key='_product_id' AND $t_posts.ID=$t_order_items.order_id)";
		

			}
		}
		return $where;
	}
	// scripts_and_styles
	public function scripts_and_styles(){
		wp_enqueue_script( 'foa-fuzzy-script', plugin_dir_url( __FILE__ ).'fuzzy-dropdown.min.js', array( 'jquery' ) );
		wp_enqueue_style( 'foa-fuzzy-styles', plugin_dir_url( __FILE__ ).'style.css' );
	}
}
