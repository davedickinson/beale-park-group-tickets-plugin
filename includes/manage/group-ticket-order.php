<?php

	/*
		Pass in Order
		// Prepare variables containing order information
		// $order_id = $query->post->ID;
	*/
	function displayOrderHTML($order, $order_id, $is_scanned, $count)
	{	
		?>
			<tr data-orderid="<?php echo $order_id;?>">

			<?php
			//var_dump($order);
			$first_name =  get_post_meta($order_id,'_billing_first_name',true);
			$last_name = get_post_meta($order_id,'_billing_last_name',true); 
			$postcode = get_post_meta($order_id,'_billing_postcode',true);
			//$visitdate = get_post_meta($order_id,'_billing_postcode',true);
			$phone = get_post_meta($order_id,'_billing_phone',true);
			$company = get_post_meta($order_id,'_billing_company',true);
			$email = get_post_meta($order_id,'_billing_organisation',true);
			$group_ticket_id = get_post_meta($order_id,'Group Ticket Number',true);

			$status = get_post_status($order_id);
			$status = apply_filters( 'woocommerce_order_get_status', 'wc-' === substr( $status, 0, 3 ) ? substr( $status, 3 ) : $status, $status );

			// Get order date as date object
			$order_date = datetime::createfromformat('Y-m-d H:i:s', $order->order_date); 
					
			// Output order information 
			?>
			<td><?php echo $count; ?></td>
			<td>#<?php echo $order_id; ?></td>
			<td><?php echo $group_ticket_id; ?></td>
			<td><?php echo $order_date->format('d/m/y H:i:s'); ?></td>
			<td><?php echo $status; ?></td>
			<td><?php echo $company; ?></td>
			<td><?php echo $first_name; ?> <?php echo $last_name; ?></td>
			<td><?php echo $phone; ?></td>
			<td><?php echo $email; ?></td>
			<td><a class="btn btn-primary" href="/wp-admin/post.php?post=<?php echo $order_id; ?>&action=edit">VIEW</a></td>
			</tr>
	<?php
	
	}

	/* Starts with / ends with */
	function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}

	function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}
?>