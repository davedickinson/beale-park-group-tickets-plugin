/*
	KLOC: Group Bookings / Group Tickets
	Author : Dave Dickinson
	Date : 2016-04-25
*/

var selectedDate = '';

function GroupTickets(config)
{
	this.dev = false;
	this.config = config;
		
	 /* Configure the dates to set the calendar at */
	this.calendarDates = this.setupCalendarDates(config);	
	this.defaultDate = moment(this.calendarDates.currentDate);
	this.ticketSeasonTypeText = "";
	
	// Get all date ranges for the seasons
	var combinedDates = config.low.dates.concat(config.high.dates);
	this.enableSeasonDates = this.getSeasonalDateRange(combinedDates);
	
	this.lowSeasonDates = this.getSeasonalDateRange(config.low.dates);
	this.highSeasonDates = this.getSeasonalDateRange(config.high.dates);
	
	// Set values here to determine what ticket parts to show and hide.
	this.groupTicketChoice = {"season":"", "proximity":""};
}

/*
	Get Group Ticket Choice
*/
GroupTickets.prototype.getGroupTicketChoice = function()
{
	return this.groupTicketChoice;
}

GroupTickets.prototype.output = function(output)
{
	if(this.dev)
	{
		console.log(output);
	}
}

GroupTickets.prototype.groupticketDate = function() 
{
	// Check if page has datepicker to prevent errors
	if ($(this.config.datepicker).length) 
	{
		var me = this;
		
		// Initialise datepicker
		$(this.config.datepicker).datepicker({ 
			dateFormat: 'dd/mm/yy',
			//gotoCurrent: true,
			defaultDate: this.defaultDate.format('DD/MM/YYYY'),
			minDate: this.calendarDates.currentDate,
			//maxDate: "+1y",
			beforeShowDay: function(date)
			{
				return me.enableSpecificDates(me, date)
			},
			showOtherMonths: true,
			dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			firstDay: 1,
			onSelect: function (date) {
				var toggleConfig = me.getGroupBookingOptions(me, date);
				me.toggleGroupBookingOptions(me, toggleConfig,date); 
			}
		});
		
		// Configure to show correct season and dates based on current selection.
		var toggleConfig = me.getGroupBookingOptions(me, this.defaultDate.format('DD/MM/YYYY'));
		me.toggleGroupBookingOptions(me, toggleConfig, this.defaultDate.format('DD/MM/YYYY')); 
		
	}
}

/*
	Toggle Group Booking Options
*/
GroupTickets.prototype.toggleGroupBookingOptions = function(me, toggleConfig, selecteddate)
{
	var seasonTarget = "";
	var proximityTarget =  "";
	
	if(toggleConfig["season"] !="")
	{
		seasonTarget = me.config[toggleConfig["season"]]["target"];
		
		if(toggleConfig["proximity"] !="")
		{
			proximityTarget = me.config[toggleConfig["season"]][toggleConfig["proximity"]];
		}
	}
	
	// TODO : Need to clear all previously selected values..
	// Hide correct season
	$(".group-ticket-container").hide();

	// Hide all previously set values.
	$(".group-ticket-container input[type=number]").val("");	
	
	// Reset total value
	$(".group-ticket-container .group-ticket-total span.total").html("");	
	
	// Show the correct seasonTarget
	$(seasonTarget).show();

	$(me.config.seasonInfo).html(me.ticketSeasonTypeText).show();
	
	// Hide all product addons within my season container
	$(".group-ticket-container .product-addon").hide();
	$(proximityTarget).show();


	// Show date of visit... 
	// Now set the value of the dateofvisit input to match this selection
	$(".product-addon-date-of-visit").show();
	
	$(me.config.dateofvisit).attr('readonly','readonly');
	$(me.config.dateofvisit).val(selecteddate);
}

/*
	Get Group Booking Options
*/
GroupTickets.prototype.getGroupBookingOptions = function(me, date)
{
	// Reset
	me.groupTicketChoice["season"] = "";
	me.groupTicketChoice["proximity"] = "";
	
	// Get Season 
	if(me.isLowSeason(me.lowSeasonDates, me.getDateFromString(date)))
	{
		me.groupTicketChoice["season"] = "low";
		me.ticketSeasonTypeText = "Low season - ";
	}
	else if(me.isHighSeason(me.highSeasonDates, me.getDateFromString(date)))
	{
		me.groupTicketChoice["season"] = "high";
		me.ticketSeasonTypeText = "High season - ";
	}
	else
	{
		me.groupTicketChoice["season"] = "";
	}
	
	// Get Proximity to Today
	var proximityToToday = me.proximityToTodayInDays(me.getDateFromString(date));
	
	if(proximityToToday < 7)
	{
		me.groupTicketChoice["proximity"] = "small";
		me.ticketSeasonTypeText+=" less than 7 days in advance";
	}
	else if(proximityToToday >=7 && proximityToToday <14)
	{
		me.groupTicketChoice["proximity"] = "medium";
		me.ticketSeasonTypeText+=" greater than 7 days and less than 2 weeks in advance";
		
	}
	else if(proximityToToday >= 14)
	{
		me.groupTicketChoice["proximity"] = "large";
		me.ticketSeasonTypeText+=" 2 weeks in advance";
	}
	else
	{
		me.groupTicketChoice["proximity"] = "";
		me.ticketSeasonTypeText+=" ";
	}
	
	return me.groupTicketChoice;
}

/*
	Is Low Season
*/
GroupTickets.prototype.isLowSeason = function(seasonDates, date)
{
	return this.isInRange(seasonDates, date);
}

/*
	Is High Season
*/
GroupTickets.prototype.isHighSeason = function(seasonDates, date)
{
	return this.isInRange(seasonDates, date);
}

/*
	Is In Range
*/
GroupTickets.prototype.isInRange = function(seasonDates, date)
{
	var isInRange = false;
	
	for(var i=0;i<seasonDates.length;i++)
	{
		var range = seasonDates[i];
		
		if(range.contains(date))
		{
			isInRange = true;
			break;
		}	
	}
	
	return isInRange;
}

/*
	Proximity to Today
*/
GroupTickets.prototype.proximityToTodayInDays = function(selectedDate)
{
	var now  = new Date();
	var nowMoment = moment(now).startOf('day');

	var diff = moment.duration(moment(selectedDate).diff(nowMoment));
	var days = parseInt(diff.asDays());

	return days;
}


/*
Calculate start Date
Move calendar to show the season month that it is currently in
*/
GroupTickets.prototype.setupCalendarDates = function()
{
	//Get Todays date
	var todayDate;

	if(typeof(this.enableSeasonDates) == "undefined")
	{
		todayDate = new Date();
		currentDate = this.getDatePlusDays(2,todayDate);
		return {"currentDate":currentDate, "todayDate":todayDate};	
	}
	
	var lastRange = this.enableSeasonDates.length;
	var count = 1;
	var inSeason = false;
	
	for(var i=0;i<this.enableSeasonDates.length;i++)
	{
		var range = this.enableSeasonDates;
		
		//Does this range contain this date?
		if(range.contains(new Date()))
		{
			todayDate = new Date();
			inSeason = true;
			break;
		}		
		else
		{	
			if(new Date() < range.start._d)
			{
				todayDate = range.start._d;
				break;
			}
	
			// Move onto the next range.
			else if(count == lastRange)
			{
				// If the current range is greater than today then use this.
				todayDate = range.start._d;
				break;	
			}
		}		
		count++;
	}
	
	var currentDate = todayDate;
	
	if(inSeason)
	{
		currentDate = this.getDatePlusDays(2,currentDate);
	}

	return {"currentDate":currentDate, "todayDate":todayDate};	
}

/*
	Get Date Plus Days
*/
GroupTickets.prototype.getDatePlusDays = function(daysToAdd, todayDate)
{
	
	// Set default date value as today - if past 12pm, default to tomorrow	
		var todayHour = todayDate.getHours(); // Get todays hour
		
		var m = moment(todayDate);
		m.add(daysToAdd, 'days'); 

		//currentDate = m.format('DD/MM/YYYY'); 
		currentDate = m.toDate();
		
		return currentDate;
}

/*
Get Date From String
This may already be in iso date format?
*/
GroupTickets.prototype.getDateFromString=function(dateString)
{
	
	//var dateString ='23/06/2015';
	var splitDate = dateString.split('/');

	var month = splitDate[1] - 1; //Javascript months are 0-11

	var date = new Date(splitDate[2], month, splitDate[0]);
	return date;
}

/*
 Get start and date dates for each date range specified for this ticket season
 convert these into moments
 */
GroupTickets.prototype.getSeasonalDateRange=function(combinedDates) {

	var enableSeasonDates = [];
	
	for(var i=0;i<combinedDates.length;i++)
	{
		var combinedDate = combinedDates[i],
		actualStartDate = "",
		actualEndDate = "";
		
		if(combinedDate.start != "")
		{
			actualStartDate = this.getDateFromString(combinedDate.start);
		}
		if(combinedDate.end != "")
		{
			actualEndDate = this.getDateFromString(combinedDate.end);
		}

		
		this.output("actualStartDate" +actualStartDate);
		this.output("actualEndDate" +actualEndDate);
			
		if(actualStartDate != "" && actualEndDate != "")
		{
			var range = moment.range(actualStartDate, actualEndDate);
			
			// Push each moment range into an array for later use.
			enableSeasonDates.push(range);	
		}
	}
	
	this.output("the result" +enableSeasonDates);
	return enableSeasonDates;
}

/*
	Is in array
*/
function isInArray(value, array) {
  return array.indexOf(value) > -1;
}


// Disable all dates within each date range
GroupTickets.prototype.enableSpecificDates=function(me, date) {
	
	me.output("in enable specific dates");
	
	var todayDate = new Date();
	var m = moment(todayDate);
	m.add(48, 'hours'); 

	for(var i=0;i<me.enableSeasonDates.length;i++)
	{
		var range = me.enableSeasonDates[i];
		
		if(range.contains(date))
		{
			// && date > m
			//console.log("range contains the date"+getJustTheDate(date));
			return [true, ''];
			break;
		}
	}

	return [false, ''];	
}
