<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); 
?>

<h2>Group admission ticket #<?php print get_post_meta($order->id, 'Group Ticket Number', true);?></h2>
<p>Please present this ticket to the entrance gate on arrival. Please also present this ticket to the train driver to claim your free train rides (subject to availibility) </p>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<?php 
 
        $items = $order->get_items(); 
 
        foreach ( $items as $item ) {
            $product_id = $item['product_id'];           
        }
 
        ?>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( array(
			'show_sku'    => false,
			'show_image'  => false,
			'$image_size' => array( 32, 32 ),
			'plain_text'  => $plain_text
		) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>
<br />
<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
<thead>
<tr>
	<th class="td" scope="col" style="text-align:left;">Group details</th>
</tr>
</thead>
<tbody>
	<tr class="">
		<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
			<?php if ($order->shipping_organisation) : ?>
			<p><strong>Group name : </strong><?php echo $order->shipping_organisation; ?></p>
			<?php endif; ?>
		</td>
	</tr>

<?php
foreach ( $items as $item_id => $item ) :
	$_product     = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
	$item_meta    = new WC_Order_Item_Meta( $item, $_product );
	$formatted_meta = $item_meta->get_formatted(true);

	if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		?>
		<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
			<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;"><?php

				// allow other plugins to add additional product information here
				do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

				// KLOC - Original
				 if (!empty( $item_meta->meta )) 
				 {
					//var_dump($item_meta->meta);
					
					foreach( $item_meta->meta as $meta_key => $meta_value)
					{
						if(strpos($meta_key, '_') !== false)
						{
							//nothing for now..
						}
						else
						{
							$meta_item_value_array = explode("-", trim($meta_key));
						
							if(count($meta_item_value_array) > 0)
							{
								echo "<br /><strong>".$meta_item_value_array[1]."</strong> : ".$meta_value[0]."";
							}
							else
							{
								echo "<br /><strong>".$meta_key."</strong> : ".$meta_value[0]."";
							}
						}	
					}
					
				 	//echo '<br/>' . nl2br( $item_meta->display( true, true, '_', "\n" ) ) . '';
				
				 }
				
				// allow other plugins to add additional product information here
				do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );

			?></td>
		</tr>
		<?php
	}
 endforeach; ?>

</tbody>
</table>
<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
