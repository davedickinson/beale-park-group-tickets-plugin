<?php
/**
 * Admin group tickets email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-grouptickets-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.5.0
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <?php

 /**
  * @hooked WC_Emails::order_details() Shows the order details table.
  * @since 2.5.0
  */
 //do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

 function admin_grouptickets_order_details($order, $sent_to_admin, $plain_text, $email)
 {
 	if ( $plain_text ) 
	{
		wc_get_template( 'emails/plain/email-order-grouptickets-details.php', array( 'order' => $order, 'sent_to_admin' => $sent_to_admin, 'plain_text' => $plain_text, 'email' => $email ) );
	} 
	else 
	{
		wc_get_template( 'emails/email-order-grouptickets-details.php', array( 'order' => $order, 'sent_to_admin' => $sent_to_admin, 'plain_text' => $plain_text, 'email' => $email ) );
	}
 }
 admin_grouptickets_order_details($order, $sent_to_admin, $plain_text, $email);
 
 /**
  * @hooked WC_Emails::order_meta() Shows order meta data.
  */
 do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
 
/*
 function admin_grouptickets_order_meta( $order, $sent_to_admin = false, $plain_text = false ) 
 {
		$fields = apply_filters( 'woocommerce_email_order_meta_fields', array(), $sent_to_admin, $order );
		
		$_fields = apply_filters( 'woocommerce_email_order_meta_keys', array(), $sent_to_admin );
		if ( $_fields ) {
			foreach ( $_fields as $key => $field ) {
				if ( is_numeric( $key ) ) {
					$key = $field;
				}
				$fields[ $key ] = array(
					'label' => wptexturize( $key ),
					'value' => wptexturize( get_post_meta( $order->id, $field, true ) )
				);
			}
		}
		if ( $fields ) {
			if ( $plain_text ) {
				foreach ( $fields as $field ) {
					if ( isset( $field['label'] ) && isset( $field['value'] ) && $field['value'] ) {
						echo $field['label'] . ': ' . $field['value'] . "\n";
					}
				}
			} else {
				foreach ( $fields as $field ) {
					if ( isset( $field['label'] ) && isset( $field['value'] ) && $field['value'] ) {
						echo '<p><strong>' . $field['label'] . ':</strong> ' . $field['value'] . '</p>';
					}
				}
			}
		}
	}
}
admin_grouptickets_order_meta($order, $sent_to_admin, $plain_text, $email);
 */
 
 /**
  * @hooked WC_Emails::customer_details() Shows customer details
  * @hooked WC_Emails::email_address() Shows email address
  */
 do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );


 /**
  * @hooked WC_Emails::email_footer() Output the email footer
 
 do_action( 'woocommerce_email_footer', $email ); */
?>

</div>
														</td>
                                                    </tr>
                                                </table>
                                                <!-- End Content -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Body -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- Footer -->
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
                                    	<tr>
                                        	<td valign="top">
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="credit">
                                                        	<?php echo wpautop( wp_kses_post( wptexturize( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) ) ) ); ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Footer -->
                                </td>
                            </tr>
							<tr>
								<td align="center" valign="top">
								
								</td>
							</tr>
							
							 <!-- Second header to show the ticket information -->
								<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
									<tr>
										<td align="center" valign="top">
											<!-- Header -->
											<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header">
												<tr>
													<td id="header_wrapper">
														<h1>For use by the train driver only</h1>
													</td>
												</tr>
											</table>
											<!-- End Header -->
										</td>
									</tr>
									<tr>
										<td align="center">
										<br />
										<!-- Train seating image -->
											<img src="<?php echo get_template_directory_uri()."/assets/train-table.png"; ?>" alt="Train ticket" />
										</td>
										
									</tr>
							</table>        
                        </table>
                    </td>
                </tr>
            </table>
		</div>
    </body>
</html>
