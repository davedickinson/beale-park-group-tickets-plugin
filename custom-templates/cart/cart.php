<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Donation add to cart form
if ($_SERVER['REQUEST_METHOD'] === 'POST'):

	$id = $_POST['id'];
	$amount = $_POST['amount']; // Donation amount by user
	$quantity = 1;

	// Check if donation
	if ($amount):
	    WC()->cart->add_to_cart($id, 1, $amount);
    endif;

endif;

	
wc_print_notices();

$current_user = wp_get_current_user();
$current_firstname = $current_user->user_firstname;
$current_lastname =  $current_user->user_lastname;

?>

<div class="row">

	<?php if(!is_user_logged_in()): ?>

		<div class="col-xs-12">
			<div class="not-logged-in">You're not logged in...
				<span class="not-logged-in-msg">
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>">
					<u>Log in</u> or <u>Create New Account</u>
					</a>
				</span>
			</div>
		</div>

	<?php else: ?>

		<div class="col-xs-12">
			<div class="not-logged-in">Hello <?php echo $current_firstname; ?> <?php echo $current_lastname; ?>
				<span class="not-logged-in-msg">
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>">
					<u>My Account</u>
					</a>
				</span>
			</div>
		</div>

	<?php endif; ?>


	<div class="col-md-8 col-xs-12">
		<div class="curved-heading">
			<p class="large">Your Basket</p>
			<p class="continue-shopping"><a href="<?php the_permalink(52); ?>" title="Continue Shopping">Continue Shopping?</a></p>
		</div>

		<?php do_action( 'woocommerce_before_cart' ); ?>

		<div class="shop-container">

		<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

			<?php do_action( 'woocommerce_before_cart_table' ); ?>

			<table class="shop_table shop_table_responsive cart" cellspacing="0">
				<thead>
					<tr>
						<th class="product-name small-heading"><?php _e( 'ITEM', 'woocommerce' ); ?></th>
						<th class="product-quantity small-heading"><?php _e( 'QUANTITY', 'woocommerce' ); ?></th>
						<th class="product-subtotal small-heading"><?php _e( 'PRICE', 'woocommerce' ); ?></th>
						<th class="product-remove">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php do_action( 'woocommerce_before_cart_contents' ); ?>

					<?php
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							?>
							<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

								<td class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
									<p><?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?></p>

									<?php $date = WC()->cart->get_item_data( $cart_item ); // Meta data ?>
									<?php echo $date; ?>
										
									<?php // Backorder notification
										if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
											echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
										}
									?>
								</td>

								<td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
									<?php
										// Disable 'quantity 1' for individually sold products
										
										// Get Categories.... 
										$product_terms = get_the_terms($product_id , 'product_cat');
										$group_ticket_match = false;
										
										foreach ($product_terms as $product_term) 
										{												
											// See if the term is in the list of terms we are looking for.
											if(strtolower($product_term->slug) == "group-ticket")
											{	
												// We have a category match
												$group_ticket_match = true;
												break;
											}
										}
														
										if ( $_product->is_sold_individually()) {
											$product_quantity = '';
										}
										else if($group_ticket_match == true)
										{
											$product_quantity = $cart_item['quantity'];
										}
										else {
											$product_quantity = woocommerce_quantity_input( array(
												'input_name'  => "cart[{$cart_item_key}][qty]",
												'input_value' => $cart_item['quantity'],
												'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
												'min_value'   => '0'
											), $_product, false );
										}

										echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
									?>
								</td>

								<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
									<?php
										echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
									?>
								</td>

								<td class="product-remove">
									<?php
										echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
											'<a href="%s" class="cart-remove" title="%s" data-product_id="%s" data-product_sku="%s">REMOVE</a>',
											esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
											__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										), $cart_item_key );
									?>
								</td>

							</tr>
							<?php
						}
					}

					do_action( 'woocommerce_cart_contents' );
					?>
					<tr>
						<td colspan="6" class="actions">

							
							<div>
								<input type="submit" class="btn-brown" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
							</div>

							<hr class="thin">

							<div class="donation-box">

								<form class="" action="" method="post">

									<?php $id = 242; // Donation product ID ?>

									<?php 
										$productFactory = new WC_PRODUCT_FACTORY();
                                    	$product = $productFactory->get_product($id);
                                    ?>

                                    <input type="hidden" name="id" value="<?php echo $id; ?>"></input>
									<h2>Donate</h2>
									<p class="medium">Beale Park is a charity, please help the park by making a donation. Thank you!</p>
									<p class="small"><i>*Donation is not compulsory</i></p>

									<span class="">
                                       Amount:
                                    </span>

                                    <select class="js-attributes" name="amount">

                                        <?php foreach ($product->get_available_variations() as $variation): ?>

                                            <?php foreach ($variation['attributes'] as $attribute): ?>
                                                <option data-price="<?php echo $variation['display_regular_price']; ?>" value="<?php echo $variation['variation_id']; ?>"><?php echo $attribute; ?></option>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>

                                    </select>

									<button class="btn-donate" type="submit">ADD DONATION</button>

								</form>

							</div>
							<hr class="thin">

							<div class="wc-proceed-to-checkout">
								<a href="<?php echo esc_url( wc_get_checkout_url() ) ;?>" class="">
									<?php echo __( 'Proceed to Checkout', 'woocommerce' ); ?>
								</a>
							</div>

							<?php do_action( 'woocommerce_cart_actions' ); ?>

							<?php wp_nonce_field( 'woocommerce-cart' ); ?>
						</td>
					</tr>

					<?php do_action( 'woocommerce_after_cart_contents' ); ?>
				</tbody>
			</table>

			<?php do_action( 'woocommerce_after_cart_table' ); ?>


			</form>
				
		</div>
	</div>

	<?php get_template_part('partials/basket-summary'); ?>


</div>

