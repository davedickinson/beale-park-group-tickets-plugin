<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
?>
<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
	<td class="product-name">
		<?php
			$is_visible = $product && $product->is_visible();

			echo apply_filters( 'woocommerce_order_item_name', $is_visible ? sprintf( '<a href="%s">%s</a>', get_permalink( $item['product_id'] ), $item['name'] ) : $item['name'], $item, $is_visible );
			echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

			do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

			//$order->display_item_meta( $item );
			
			/*----------- KLOC Start long winded way ---------*/
			
				$product   = $order->get_product_from_item( $item );
				$item_meta = new WC_Order_Item_Meta( $item, $product );
				$output         = '';
				$formatted_meta = $item_meta->get_formatted(true);
				$flat = false;
				$return = false;
				
				if ( ! empty( $formatted_meta ) ) {
					$meta_list = array();

					foreach ( $formatted_meta as $meta ) {
						if ( $flat ) {
							$meta_list[] = wp_kses_post( $meta['label'] . ': ' . $meta['value'] );
						} else {
							$meta_list[] = '
								<dt class="variation-' . sanitize_html_class( sanitize_text_field( $meta['key'] ) ) . '">' . wp_kses_post( $meta['label'] ) . ':</dt>
								<dd class="variation-' . sanitize_html_class( sanitize_text_field( $meta['key'] ) ) . '">' . wp_kses_post( wpautop( make_clickable( $meta['value'] ) ) ) . '</dd>
							';
						}
					}

					if ( ! empty( $meta_list ) ) {
						if ( $flat ) {
							$output .= implode( $delimiter, $meta_list );
						} else {
							$output .= '<dl class="variation">' . implode( '', $meta_list ) . '</dl>';
						}
					}
				}

				$output = apply_filters( 'woocommerce_order_items_meta_display', $output, $this );

				if ( $return ) {
					return $output;
				} else {
					echo $output;
				}
			
			/*-------------------- End -------------------------*/
			
			$order->display_item_downloads( $item );

			do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
		?>
	</td>
	<td class="product-total">
		<?php echo $order->get_formatted_line_subtotal( $item ); ?>
	</td>
</tr>
<?php if ( $show_purchase_note && $purchase_note ) : ?>
<tr class="product-purchase-note">
	<td colspan="3"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>
</tr>
<?php endif; ?>
