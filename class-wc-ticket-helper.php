<?php
//http://www.portmanteaudesigns.com/blog/2015/02/04/woocommerce-custom-checkout-fields-email-backend/
//https://wisdmlabs.com/blog/add-custom-data-woocommerce-order/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A custom Ticket Helper class
 *
 * @since 0.1
 */
 
class WC_TicketHelper {

	/**
     * Holds the values to be used in the fields callbacks
     */
	 
    private $options;
	private $group_ticket_prefix = "c";
	private $group_ticket_number = 6000;
	private $group_ticket_minimum_numbers = 15;
	
	public function __construct() 
	{
		$this->options = get_option('kloc_ticket_manager');
	}
	
	/*-----------------------
		Season Date Ranges
	------------------------*/
	
	/*------------------------------------
		High Season
	--------------------------------------*/
	public function getHighSeasonDateRange(){
		$highseason = isset( $this->options["highseason_daterange"]) ?  $this->options["highseason_daterange"] : '';
		return $highseason;
	}
	
	public function getHighSeasonDateRangeJSON()
	{
		$highseason = $this->getHighSeasonDateRange();
		echo json_encode($highseason, JSON_UNESCAPED_SLASHES);
	}
	
	public function getHighSeasonDateRangeHTML(){
		
		$highseason = $this->getHighSeasonDateRange();
		
		if($highseason !="")
		{
			foreach ($highseason as $key => $value) 
			{	
				$start_value = isset($value["start"]) ? esc_attr( $value["start"]) : '';
				$end_value = isset($value["end"]) ? esc_attr( $value["end"]) : '';
			
				print "<div style='display:none' class='seasonal-daterange' data-season='high' data-id='$key'>";
				print "<div class='start'>".$start_value."</div>";
				print "<div class='end'>".$end_value."</div>";
				print "</div>";
			}
		}
	}
	
	/*------------------------------------
		Low Season
	--------------------------------------*/
	public function getLowSeasonDateRange(){
		$lowseason = isset( $this->options["lowseason_daterange"]) ? $this->options["lowseason_daterange"] : '';
		return $lowseason;
	}
	
	public function getLowSeasonDateRangeJSON()
	{
		$lowseason = $this->getLowSeasonDateRange();
		echo json_encode($lowseason,JSON_UNESCAPED_SLASHES);
	}
	
	public function getLowSeasonDateRangeHTML(){
		
		$lowseason = $this->getLowSeasonDateRange();
		if($lowseason != "")
		{
			foreach ($lowseason as $key => $value) 
			{
				$start_value = isset($value["start"]) ? esc_attr( $value["start"]) : '';
				$end_value = isset($value["end"]) ? esc_attr( $value["end"]) : '';
			
				print "<div style='display:none' class='seasonal-daterange' data-season='low' data-id='$key'>";
				print "<div class='start'>".$start_value."</div>";
				print "<div class='end'>".$end_value."</div>";
				print "</div>";
			}
		}
	}
	
	/*------------------------------------
		Get Group Ticket Minimum Numbers
	--------------------------------------*/
	public function getGroupTicketMinimumNumbers()
	{
		return $this->group_ticket_minimum_numbers;
	}
	
	
	/*-------------------------------------
		Get Ticket Categories
	-----------------------------------*/
	public function getGroupTicketCategories()
	{	
		$group_ticket_categories = isset( $this->options['group_ticket_categories'] ) ? esc_attr( $this->options['group_ticket_categories']) : '';
		$group_ticket_categories_array = array();
		
		if($group_ticket_categories != "")
		{
			$group_ticket_categories_array = explode(",", $group_ticket_categories);
			$group_ticket_categories_array = array_map('trim', $group_ticket_categories_array); // Trim the values
			$group_ticket_categories_array = array_map('strtolower', $group_ticket_categories_array); // lower case too!
		}
		
		$array = array("group_ticket_categories" => $group_ticket_categories_array);
		return $array; 
	}
	
	/*-------------------------------------
		Get Ticket Product Data
	-----------------------------------*/
	public function getGroupTicketProductData()
	{	
		$product_sku = isset( $this->options['group_ticket_skus'] ) ? esc_attr( $this->options['group_ticket_skus']) : '';
		$product_sku_array = array();
		
		if($product_sku != "")
		{
			$product_sku_array = explode(",", $product_sku);
			$product_sku_array = array_map('trim', $product_sku_array); // Trim the values
			$product_sku_array = array_map('strtolower', $product_sku_array); // lower case too!
		}
		
		$array = array("product_sku" => $product_sku_array);
		return $array; 
	}
	
	/*-------------------------------------
		Get Ticket Surcharge options
	-----------------------------------*/
	public function getGroupTicketSurcharge()
	{
		// Product SKU
		$product_sku = isset( $this->options['group_ticket_extrafee_product'] ) ? esc_attr( $this->options['group_ticket_extrafee_product']) : '';
		$product_sku_array = array();
		
		if($product_sku != "")
		{
			$product_sku_array = explode(",", $this->options['group_ticket_extrafee_product']);	
			$product_sku_array = array_map('trim', $product_sku_array); // Trim the values
			$product_sku_array = array_map('strtolower', $product_sku_array); // lower case too!
		}
		
		// Product Option Name
		$product_option_name = isset( $this->options['group_ticket_extrafee_product'] ) ? esc_attr( $this->options['group_ticket_extrafee_product']) : '';
		$product_option_name_array = array();

		if($product_option_name != "")
		{
			$product_option_name_array = explode(",", $this->options['group_ticket_extrafee_product_option']);
			$product_option_name_array = array_map('trim', $product_option_name_array); // Trim the values
			$product_option_name_array = array_map('strtolower', $product_option_name_array); // Lower case too!
		}
		
		// Amount
		// Default to zero
		$amount = isset( $this->options['group_ticket_extrafee_amount'] ) ? esc_attr( $this->options['group_ticket_extrafee_amount']) : 0;

		$array = array("product_sku" => $product_sku_array, "product_option" =>$product_option_name_array , "amount"=>$amount);
		
		return $array; 
	}


	/*------------------------------------------------------------------
		Get group ticket Number - which is actually not a number!
	----------------------------------------------------------------*/
	public function getNextGroupTicketNumber()
	{
		
		// Get the values - if they are empty then set them to the defaults.
		$group_ticket_prefix = isset( $this->options['group_ticket_prefix'] ) ? esc_attr( $this->options['group_ticket_prefix']) : $this->group_ticket_prefix;
		$group_ticket_number = isset( $this->options['group_ticket_number'] ) ? esc_attr( $this->options['group_ticket_number']) : $this->group_ticket_number;
	
		// Get the current number - add 1 to it then save it then return it.
		$next_group_ticket_number = $group_ticket_number+1;
		
		$this->options['group_ticket_number'] = $next_group_ticket_number;
		
		// Ensure this will not overwrite the settings.
		//update_option('kloc_ticket_manager', $next_group_ticket_number);
		
		update_option('kloc_ticket_manager', $this->options);
		
		return $group_ticket_prefix."".$next_group_ticket_number; 
	}
	
	/*---------------------------------
	Has been set
	-----------------------------------*/
	public function hasBeenSet($product_id, $key_to_match)
	{
		$has_been_set = false;
		
		foreach ($_REQUEST as $itemkey=> $itemvalue) 
		{	
			// Horrible string in string match
			if( strpos(strtolower((string)$itemkey), strtolower((string)$product_id)) !== false)
			{
				if(is_array($itemvalue))
				{
					foreach ($itemvalue as $key => $value) 
					{
						if($key == $key_to_match)
						{
							if($value != "")
							{
								$has_been_set = true;
								break;
							}
						}
					}			
				}
			}
		}
		return $has_been_set;
	}
	
	
	/*-----------------------------------
		Get Numbers in Group
	-----------------------------------*/
	public function getNumbersInGroup()
	{
		$child_under_2= "child-under-2";
		$child_over_2 ="child-over-2";
		$adult = "adult";
		$disabled = "disabled";
		$product_option_items = array($child_under_2, $child_over_2, $adult, $disabled);
			
		$total_places = 0;
			
			foreach ($_REQUEST as $item) 
			{	
				if(is_array($item))
				{
					foreach ($item as $key => $value) 
					{
						if(in_array($key, $product_option_items))
						{
							if($value != "")
							{
								$int_value = intval($value);
								$total_places+=$int_value;
							}
						}
					}			
				}
			}
			
			return $total_places;
		}
} // end \WC_TicketHelper class
