<?php /* Template Name: Gift Shop */ ?>

<?php 

if ($_SERVER['REQUEST_METHOD'] === 'POST'):

	$cart_added_qty = $_POST['quantity'];
	$cart_added_id = $_POST['id'];
	$cart_added_title = get_the_title($cart_added_id);

    if ($cart_added_qty > 0):

    	$result = WC()->cart->add_to_cart($cart_added_id, $cart_added_qty);

    endif;

endif;
	
?>

<?php get_header(); ?>

 <div class="container content-container woocommerce">
	<div class="row">
		<div class="col-md-3 col-xs-12"><?php get_template_part('partials/sidebar'); ?></div>

		<div class="col-md-9 col-sm-12 col-xs-12">

		<?php if ($result): ?>

			<div class="woocommerce-message success-product">
				<?php echo $cart_added_title; ?> x<?php echo $cart_added_qty; ?> added to the basket.
				<span><a href="<?php echo get_the_permalink(58); ?>" title="Open Basket">Open Basket</a></span>
			</div>

        <?php elseif (!$result && !is_null($result)): ?>
        		<?php echo get_template_part('woocommerce/notices/notice.php'); ?>
			<ul class="woocommerce-error">
				<li>Sorry, there was a problem adding this item to the basket</li>
			</ul>
	
        <?php endif; ?>
        	<div class="row">
        		<div class="col-xs-12">
					<div class="shop-filter">
							
						<p>SHOWING:</p>

						<div class="select-menu small">		
	          				
	          			<?php

                            $selected_category = $_GET['cat'];

                            if($selected_category):
                                $selected_category_name = get_term_by( 'slug', $selected_category, 'product_cat');
                                $selected_category_name = $selected_category_name->name;
                                $selected_category_slug = $selected_category_name->slug;
                            endif;

                            $args = array(
                              'taxonomy' => 'product_cat',
							  'exclude' => array(20,21,22,23,29,41),
							);

                            $categories = get_categories($args);
                              
                            $select = "<select name='cat' id='cat' class='category js-category'>";
                             
                            $select.= "<option data-link=\"".get_permalink(52)."\" value='ALL GIFTS'>ALL GIFTS</option>";

                            foreach($categories as $category){
                               
                                $select.= "<option data-link=\"".get_permalink(52)."?cat=".$category->slug."\" value='".$category->slug."'>".$category->name."</option>";
                               
                            }
                              
                            $select.= "</select>";
                              
                            echo $select;
                     ?>	

                     		<!-- Show selected category -->
		                    <script type="text/javascript">

							var currentCat = '<?php echo $selected_category; ?>';

							jQuery('.small option').each(function(){
							   if (jQuery(this).val() == currentCat) 
							   {
							       jQuery(this).attr("selected", "selected");
							   }
							});

							</script>


							
						</div>
					</div>
				</div>
        	</div>
			<div class="row products">
			
			

				<?php

				if($selected_category_name): 

					$args = array(
						'post_type' => 'product',
						'posts_per_page' => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'product_cat',
								'field' => 'name',
								'terms' => $selected_category_name,
								'operator' => 'IN',
							),	
						),
					);

				else:
					
					$args = array(
						'post_type' => 'product',
						'posts_per_page' => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'product_cat',
								'field' => 'slug',
								'terms' => array( 'tickets', 'book-tickets', 'donation','Group Ticket'), // Exclude ticket and donation products
								'operator' => 'NOT IN',
							),
						),
					);

				endif;

					$loop = new WP_Query( $args );
					
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) : $loop->the_post(); 

						$productID = get_the_ID();
						$products = new WC_Product_Factory();  
						$product = $products->get_product($productID); 

						$product_stock = $product->get_stock_quantity();

						$price = get_post_meta($productID, '_regular_price', true);
						$saleprice = get_post_meta($productID, '_sale_price', true);
						$description = get_field('prod_description');
				
						?>
				



							<div class="col-lg-4 col-md-6 col-sm-6">
								<form class="" action="" method="post">	
								<div class="product">

									<input type="hidden" name="id" value="<?php echo $productID; ?>">

								
									<div class="product-image">
										<?php echo $product->get_image('shop_thumbnail'); ?>
									</div>	
									
									<div class="product-content">

										<span class="product-title"><?php the_title(); ?></span>

										<?php if ( $price = $product->get_price_html() ) :
											echo $price;

										endif; ?>

										<div class="product-desc"><p><?php echo $description; ?></p></div>

										<div class="product-footer">
											<div class="quantity">
												<span class="product_quantity_minus">-</span>
													<input type="number" step="1" min="1" max="999" name="quantity" value="1" title="Qty" class="input-text qty text" size="4">
												<span class="product_quantity_plus">+</span>
											</div>

										<?php if($product_stock === 0): ?>

											<button class="btn-red product-add-to-cart" type="submit" disabled>Out of Stock</button>

										<?php else: ?>
											<button class="btn-green product-add-to-cart" type="submit">Add to Basket</button>

										<?php endif; ?>
										</div>
									
									</div>
								</div>
								</form>
							</div>

						

               				<?php

						
						endwhile;
					} else {
						echo __( 'No products found' );
					} ?>

				<?php wp_reset_postdata(); ?>

			</div>



		</div>
	</div>
</div>

<?php get_footer(); ?>