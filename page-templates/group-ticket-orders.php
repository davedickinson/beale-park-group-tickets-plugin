<?php 

/* Template Name: Group Ticket Orders */

// Redirect to login page if user is not logged in
if ( !is_user_logged_in() ) {
   auth_redirect();
}

$capability = true;

// Check if user has custom capability to access this page
// $capability = current_user_can('access_manage_group_tickets'); 

if($capability):
?>


<!DOCTYPE html>

<html class="push" <?php language_attributes(); ?>>
  <head>
    <title><?php wp_title(); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php wp_head(); ?>

	    <script>
		  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  // ga('create', 'UA-3093735-41', 'auto');
		  // ga('send', 'pageview');
		</script>

	</head>

  <body <?php body_class(isset($class) ? $class : ''); ?>>

	<!-- Header -->
	<header class="full-width">
	<div class="droop hidden-xs">
        <a href="http://bealepark.org.uk"><img src="<?php echo get_template_directory_uri(); ?>/img/beale-park-logo.png" class="img-responsive" /></a>
	</div>
		<div class="container">
        <a href="<?php echo wp_logout_url(); ?>" class="lo-btn">Logout</a>
			<div class="row">
			  	<div class="col-md-1 col-xs-6">
                    <a href="http://bealepark.org.uk"><div class="logo-mobile"><img src="<?php echo get_template_directory_uri(); ?>/img/beale-park-logo-mobile.png" /></div></a>
                   
			  	</div>
	
			</div>	
		</div>
	</header>
<body>
  
	<div class="container">
	  <div class="row">

	  	<div class="col-md-12">
	  		<br/>
	  		<p><strong>Showing Group Tickets:</strong> </p>

	    	<?php 
	    	// Hide orders that are not processing or completed
			$post_status = array( 'wc-processing', 'wc-completed' );

			// WP_Query arguments
			$args = array (
				'post_type'              => array( 'shop_order' ),
				'post_status' 			 => $post_status,
				'posts_per_page'		 => '-1',
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'Group Ticket Number',
						'value' => 'c',
						'compare' => 'LIKE'
					)
				));

					// The Query
					$query = new WP_Query($args);

						// The Loop
						if ($query->have_posts())
						{
							while ($query->have_posts())
							{
								$query->the_post();
								$order = new WC_Order($query->post->ID);
								displayOrderHTML($order, $query->post->ID, $is_scansearch);
							}
						}
						else
						{
							?><p><?php _e( 'Sorry, no group tickets matched your criteria.' ); ?></p><?php
						}

						// Restore original Post Data
						wp_reset_postdata(); 
				?>
	    </div>

	  </div>
	</div>

<?php else: ?>
	<p style="text-align: center; margin-top: 50px;"><?php echo 'Access denied'; ?></p>
<?php endif; ?>

</body>
</html>