<?php
/**
 * Template Name: Group Ticket
 *
 * @package WordPress
 * @subpackage Beale Park
 */
 
get_header('shop'); 

?>
<style>

#group-tickets .woocommerce-tabs{ display:none;}
#group-tickets .related {display:none;}
#group-tickets .group-ticket-container{min-height:500px;position:relative;}

#group-tickets .clear-fix{clear:both;display:block;}
#group-tickets .images{display:none;}
#group-tickets .summary{float:none;width:100%;}
#group-tickets .quantity{display:none;}
#group-tickets p.price{display:none;}
#group-tickets .addon-alert{display:none;}
#group-tickets .product_meta{display:none;}
#group-tickets h3{display:none;}

#group-tickets .woocommerce form .form-row input.input-text, .woocommerce form .form-row textarea {width:60%;}
#group-tickets .group-ticket-total{position:absolute;right:0;bottom:4%;width:20%;background:#fff;height:200px;}
#group-tickets .group-ticket-total span.total{font-size:18px;font-weight:bold;}

/* Hide on load
#group-tickets .group-ticket-container {display:none;}*/

</style>
<!--<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" media="all" /> -->
<link rel="stylesheet" href="../wp-content/plugins/kloc-ticket-manager/styles/jquery-ui-timepicker-addon.css" type="text/css" media="all" /> 

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>

<script src="../wp-content/plugins/kloc-ticket-manager/js/jquery-ui-sliderAccess.js"></script>
<script src="../wp-content/plugins/kloc-ticket-manager/js/jquery-ui-timepicker-addon.js"></script>
<!--<script src="../wp-content/plugins/kloc-ticket-manager/js/moment.js"></script>
<script src="../wp-content/plugins/kloc-ticket-manager/js/moment-range.js"></script>-->
<script src="../wp-content/plugins/kloc-ticket-manager/js/groupticket-date.js"></script>

<?php
	$ticket_helper = new WC_TicketHelper();
?>

<script type="text/javascript">

var $ = jQuery.noConflict();
var groupTickets;

jQuery(function($){
	
	// Setup moment locale
	moment.locale('en-gb');
	
	/* Set Group Ticket Date Picker */
	var config = {
		"datepicker":"#group-ticket-datepicker",	
		"dateofvisit":".product-addon-date-of-visit input",
		"seasonInfo":".season-info",
		"low":{
			"dates": <?php $ticket_helper->getLowSeasonDateRangeJSON();?>, 
			"target":"#group-ticket-low-season",
			"small":".product-addon-6-days-in-advance", 
			"medium":".product-addon-7-to-13-days-in-advance", 
			"large":".product-addon-14-days-in-advance",
			"total":".group-ticket-low-season-total span.total"
		},
	"high":{
		"dates": <?php $ticket_helper->getHighSeasonDateRangeJSON();?>, 
		"target":"#group-ticket-high-season", 
		"small":".product-addon-6-days-in-advance",
		"medium":".product-addon-7-to-13-days-in-advance", 
		"large":".product-addon-14-days-in-advance",
		"total" :".group-ticket-high-season-total span.total"
		}
	};
	
	groupTickets = new GroupTickets(config);
	groupTickets.groupticketDate();
	
	// Clone the woocommerce-message it has a habit of hiding.
	if($(".woocommerce-message").length >0)
	{	
		if(!$(config.low.target +" .woocommerce-message").length > 0)
		{
			$(config.low.target +" .single-product").prepend($(".woocommerce-message").clone());
		}
		if(!$(config.high.target +" .woocommerce-message").length > 0)
		{
			$(config.high.target +" .single-product").prepend($(".woocommerce-message").clone());
		}
	}
	
	// Clone the woocommerce-message it has a habit of hiding.
	if($(".woocommerce-error").length >0)
	{
		if(!$(config.low.target +" .woocommerce-error").length > 0)
		{
			$(config.low.target +" .single-product").prepend($(".woocommerce-error").clone());
		}
		if(!$(config.high.target +" .woocommerce-error").length > 0)
		{
			$(config.high.target +" .single-product").prepend($(".woocommerce-error").clone());
		}
	}
	
	// Hijack the form
	$("form").submit(function(){
		//console.log("about to submit form");
		return true;
	});
	
	//Reset all inputs on the page to be "";
    jQuery('.addon-input_multiplier').val("");
	jQuery('.product-addon-date-of-visit').show();
	
	// High season
	$('#group-ticket-high-season input[type=number]').bind("change keyup mouseup", function() 
	{ 
		// Get the currently active pane
		recalculateTotal("#group-ticket-high-season input[type=number]", ".group-ticket-high-season-total span.total");
	});
	
	// Low season
	$('#group-ticket-low-season input[type=number]').bind("change keyup mouseup", function() 
	{ 
		// Get the currently active pane.
		recalculateTotal("#group-ticket-low-season input[type=number]", ".group-ticket-low-season-total span.total");
	});
	
});


function recalculateTotal(inputsTarget, totalTarget)
{
	var totalValue = 0;
	
	$(inputsTarget).each(function(index, element)
	{
		if($(element).val() !="" && $(element).attr("data-price") != "")
		{
			var price = parseFloat($(element).attr("data-price"));
			var qty = parseFloat($(element).val());
			var subTotal = parseFloat(price * qty);
			totalValue+= parseFloat(subTotal);
		}
	});
	
	// set the value 
	$(totalTarget).html("&pound;"+parseFloat(totalValue).toFixed(2));
}
	
</script>

<div class="container">
	<div class="row">
		<div class="col-md-8 col-sm-12 col-xs-12">
			<div class="curved-heading"><p>Book your group tickets</p></div>
				<div class="shop-container book-tickets">
					<div id="group-tickets">
					
						<div id="group-ticket-datepicker"></div>
						<h3 class="season-info"></h3>
						<div id="group-ticket-high-season" class="group-ticket-container">
						<?php echo do_shortcode('[product_page sku="group-ticket-high-season"]'); ?>
						<div class="clear-fix"></div>
						
						<div class="group-ticket-high-season-total group-ticket-total">
						<h2>Total</h2>
						<span class="total">
						�0.00
						</span>
				
						</div>
						
						
						</div>
						<div class="clear-fix"></div>
						<div id="group-ticket-low-season" class="group-ticket-container">
						<?php echo do_shortcode('[product_page sku="group-ticket-low-season"]'); ?>
						<div class="group-ticket-low-season-total group-ticket-total">
						<h2>Total</h2>
						<span class="total">
						�0.00
						</span>
						</div>
						<div class="clear-fix"></div>
						</div>
					</div>
				</div>
		</div>
	
		<?php get_template_part('partials/basket-summary'); ?>

	</div>
</div>