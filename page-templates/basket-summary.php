<div class="col-md-4 col-sm-12 col-xs-12">
    <div class="basket-summary-container">
		<div class="curved-heading">
			<p class="medium">Basket Summary</p>
		</div>

		<div class="shop-container basket-summary">
			<div class="basket-item-summary">
				<?php
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ):
					$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key ); 

					$product_name = $_product->get_title();

					// Add HR above each ticket to seperate items on basket summary
					if (strpos($product_name, 'Ticket') !== FALSE) {
					    echo '<hr/>';
					}
					
					if($_product->is_sold_individually()):
						$product_qty = '';
					else:
						$product_qty = 'x'.$cart_item['quantity'];
					endif;?>

					<p><?php echo $product_name; ?> <?php echo $product_qty; ?>
					
					<span class="subtotal pull-right">
						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
					</span>

						<?php $date = WC()->cart->get_item_data( $cart_item ); // Meta data ?>

					<?php echo $date; ?>

					</p>

				<?php endforeach; ?>
			</div>
			<hr class="thin">
			<div class="cart-collaterals woocommerce-cart">
			
				<?php do_action( 'woocommerce_cart_collaterals' ); ?>
			</div>

			<?php do_action( 'woocommerce_after_cart' ); ?>
		</div>
    </div>
	</div>