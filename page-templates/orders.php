<?php 

/* Template Name: Order Interface */

// Redirect to login page if user is not logged in
if ( !is_user_logged_in() ) {
   auth_redirect();
}

acf_form_head();

get_header(); 

// Check if user has custom capability to access this page
$capability = current_user_can('access_manage_tickets'); 

if($capability):

	include(locate_template('qrcode/qrcode.php'));

	/*----------------------------------
		Set values from POST / GET array
		{47C2049B-CF6C-4312-C457-D376251D6B7F}
	------------------------------*/
	$searchterm = "";

	if(isset($_GET['searchterm']))
	{
		$searchterm = $_GET['searchterm'];
	}

	// Override the search term.
	if(isset($_POST['revised_searchterm']))
	{
		$searchterm = $_POST['revised_searchterm'];
	}
	
	/*------------------------------
	Save Details from the ticket
	--------------------------------*/
	
	$order_id =  $_POST['order_id']; // int
	$line_item =  $_POST['line_item']; // array
	$ticket_admitted = $_POST['ticket_admitted']; //array
	$is_scansearch = false;

	updateAdmitted($order_id, $line_item, $ticket_admitted); // update ticket status admitted

	$notes = $_POST['notes'];
	update_field('field_56b8b34fd01cb', $notes, $order_id); // update notes field

	?>

  	<!-- Header -->
	<header class="full-width">
	<div class="droop hidden-xs">
        <a href="http://bealepark.org.uk"><img src="<?php echo get_template_directory_uri(); ?>/img/beale-park-logo.png" class="img-responsive" /></a>
	</div>
		<div class="container">
        <a href="<?php echo wp_logout_url(); ?>" class="lo-btn">Logout</a>
			<div class="row">
			  	<div class="col-md-1 col-xs-6">
                    <a href="http://bealepark.org.uk"><div class="logo-mobile"><img src="<?php echo get_template_directory_uri(); ?>/img/beale-park-logo-mobile.png" /></div></a>
                   
			  	</div>
			  	<div class="col-md-11 col-xs-6">
					<?php get_template_part('searchform-orders'); ?>
				</div>
				<br />
			</div>	
		</div>
	</header>
	
	
	<div class="container">
	  <div class="row">

	  	<div class="col-md-12">
	  		<br/>
	  		<p><strong>Results for:</strong> <?php echo $searchterm; ?></p>

	    	<?php 
	    	// Hide orders that are not processing or completed
			$post_status = array( 'wc-processing', 'wc-completed' );

			// WP_Query arguments
			$args = array (
				'post_type'              => array( 'shop_order' ),
				'post_status' 			 => $post_status,
				'posts_per_page'		 => '-1',
			);

			if($searchterm != "") {

				$query="";

				// Use different search method if # is provided
				if (strpos($searchterm, '#') !== FALSE)
				{
					$orderId = 0;
					$orderIdArray = explode('#', $searchterm);

					if(count($orderIdArray) > 0)
					{
						$orderId = $orderIdArray[1];
					}

					if($orderId != 0)
					{
						$query = wc_get_order($orderId);

						displayOrderHTML($query, $orderId, false);
					}
				}
				else
				{
					// Search through the custom post types.
					// Add search term to WP Query arguments
					$args["s"] = $searchterm;

					//if (strpos($searchterm, '{') !== FALSE && strpos($searchterm, '}') !== FALSE)
					if(startsWith($searchterm, '{') && endsWith($searchterm, '}'))
					{
						$is_scansearch = TRUE;
					}

					// The Query
					$query = new WP_Query($args);

						// The Loop
						if ($query->have_posts())
						{
							while ($query->have_posts())
							{
								$query->the_post();
								$order = new WC_Order($query->post->ID);

								displayOrderHTML($order, $query->post->ID, $is_scansearch);
							}
						}
						else
						{
							?><p><?php _e( 'Sorry, no tickets matched your criteria.' ); ?></p><?php
						}

						// Restore original Post Data
						wp_reset_postdata(); 
				}
			}
				?>
	    </div>

	  </div>
	</div>

<?php else: ?>
	<p style="text-align: center; margin-top: 50px;"><?php echo 'Access denied'; ?></p>
<?php endif; ?>

<?php

/*-----------------------------
	Ticket Functions
------------------------------*/

	/* 
	Check if order contains tickets.
	*/
	function containsTicket($order)
	{
		$containsTicket = false;

		// Loop through all products on the order	
	    foreach ($order->get_items() as $key => $lineItem)
	    {
			
	    	// TICKET product
			if($lineItem['product_id'] == 124 || $lineItem['product_id'] == 131)
			{
				 $containsTicket = true;
				 break;
			}
		}		
		return $containsTicket;
	}                       

	
	/*----------------------
		Can Admit Ticket.
	-----------------------*/

	function canAdmitTicket($ticketDate, $todayDate, $admitByDateCutOff, $admitted)
	{
		$is_valid = false;
		
		//print "this is the cut off date ".$admitByDateCutOff->format('Y-m-d')."<br />";
		//print "is ".$ticketDate->format('Y-m-d')." less than ".$admitByDateCutOff->format('Y-m-d')."<Br />";
		//print " and is ".$ticketDate->format('Y-m-d')." greater than ".$todayDate->format('Y-m-d')."<br />";
		
		// Check that ticket is still valid, this includes todays date
		// ticket date is within ranges greater than today <= today + 2 days!
		if(strtotime($ticketDate->format('Y-m-d')) <= strtotime($admitByDateCutOff->format('Y-m-d')) 
			&& strtotime($ticketDate->format('Y-m-d')) >= strtotime($todayDate->format('Y-m-d')))
		{
			if($admitted != true)
			{
				//print "Ticket is valid and not admitted"."<br  />";
				$is_valid = true;
			}
		}
		elseif(strtotime($ticketDate->format('Y-m-d')) < strtotime($todayDate->format('Y-m-d'))){
			//print "Ticket is invalid"."<br  />";
			$is_valid = false;
		}
		
		//print "about to return ticket is: ".$is_valid." <br />";
			
		return $is_valid;
	}
	
	/*
		Pass in Order
		// Prepare variables containing order information
		// $order_id = $query->post->ID;
	*/
	function displayOrderHTML($order, $order_id, $is_scanned)
	{
		if(containsTicket($order) == true)
		{
			?>
			<form action="" method="POST" id="<?php echo $order_id;?>">
			<div class="ticket-panel" data-orderid="<?php echo $order_id;?>" data-isscanned="<?php echo $is_scanned?>">

			<?php
				//var_dump($order);
			$first_name =  get_post_meta($order_id,'_billing_first_name',true);
			$last_name = get_post_meta($order_id,'_billing_last_name',true); 
			$postcode = get_post_meta($order_id,'_billing_postcode',true);
			$phone = get_post_meta($order_id,'_billing_phone',true);
			$email = get_post_meta($order_id,'_billing_email',true);

			$status = get_post_status($order_id);
			$status = apply_filters( 'woocommerce_order_get_status', 'wc-' === substr( $status, 0, 3 ) ? substr( $status, 3 ) : $status, $status );

			$validStatus = false;

			if($status == 'processing' || $status == 'completed') {
				$validStatus = true;
			} 

			// Get order date as date object
			$order_date = datetime::createfromformat('Y-m-d H:i:s', $order->order_date); 
			
			// Work out the earliest date that ticket can be admitted
			$ticketEarliestAvail = datetime::createfromformat('Y-m-d H:i:s', $order->order_date); 
			$ticketEarliestAvail->add(new DateInterval('PT72H')); // Add 72 hours to order date 

			// Output order information 
			?>
			<p><strong>Order ID: </strong><?php echo $order_id; ?></p>
			<p><strong>Order Date: </strong><?php echo $order_date->format('d/m/y H:i:s'); ?></p>

			<?php if($validStatus): ?>
				<p><strong>Order Status: </strong><span class="success"><?php echo $status; ?></span></p>
			<?php else: ?>
				<p><strong>Order Status: </strong><span class="error"><?php echo $status; ?></span></p>
			<?php endif; ?>

			<hr>

			<div class="row" data-ticketqr="">
				<div class="col-xs-6">
					<p><strong>Name: </strong><?php echo $first_name; ?> <?php echo $last_name; ?></p>
					<p><strong>Postcode: </strong><?php echo $postcode; ?></p>
					<p><strong>Phone: </strong><?php echo $phone; ?></p>
					<p><strong>Email: </strong><?php echo $email; ?></p>
				</div>
				<div class="col-xs-6">

				<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">

				<?php
		
				$count = 0;

				// Loop through all products on the order	
			    foreach ($order->get_items() as $key => $lineItem): 
					
			    	$admitted = $lineItem['Admitted']; // Get admitted value (0/1)
			    	
					// Decide whether or not this ticket can be admitted.
					$canAdmit = false;
					
			    	// TICKET product
					if($lineItem['product_id'] == 124 || $lineItem['product_id'] == 131): // if product is a ticket high season or ticket low season
						$line_item_count = $count;
						?>

					<?php	
						/*	
						As per the spec, we are loosly checking these conditions:

						1.	If people book tickets far in advance but then
							decide to turn up early (Has to be 72 hours
							after booking the ticket) there will be a facility
							to manually check them in.

						2.	Tickets bought online are only valid for the date specified and 2 days after

						Update - part 1 no longer required.
						*/

						$todayDate = new DateTime('now'); // Get Todays Date
						$ticketDateStr = $lineItem['Date']; // Get Ticket Date
						
						// Make date string into date (we need to specify the incoming format of the string first)	
						$ticketDate = datetime::createfromformat('d/m/yy', $ticketDateStr);
						
						// Admit by date cut off - was P2D
						$admitByDateCutOff = new DateTime('now');
						$admitByDateCutOff->add(new DateInterval('PT48H'));
																															
						// Ticket valid until - determine whether we need to add the 2 days here as well??
						$ticketValidUntil = datetime::createfromformat('d/m/yy', $ticketDateStr);
						$ticketValidUntil->add(new DateInterval('PT48H')); // Add 2 days to ticket date

						// We are only allowing tickets between today and 2 days ahead
												
						// Check to see if the ticket can be admitted.
						$canAdmit = canAdmitTicket($ticketDate, $todayDate, $admitByDateCutOff, $admitted);
						
						?>
						<hr>

						<?php
					
						if($is_scanned)
						{
						?>
							<div class="row" data-isscanned="true">
						<?php
						}
						else
						{
						?>
							<div class="row" data-isscanned="false">
						<?php
						}
						?>
							<div class="col-xs-6">
								<p><strong>Ticket Date: </strong><?php echo $ticketDateStr; ?>

									<?php // Check that ticket is still valid, this includes todays date
									// It is still valid if it is in the future - this does not show if it is suitable to be admitted.
									if(strtotime($ticketValidUntil->format('Y-m-d')) >= strtotime($todayDate->format('Y-m-d'))): ?>

										<span class="success">(Valid) </span>
										
									<?php elseif(strtotime($ticketValidUntil->format('Y-m-d')) < strtotime($todayDate->format('Y-m-d'))): ?>

										<span class="error">(Expired)</span>

									<?php endif; ?>
									
									<?php
										if($canAdmit && $validStatus)
										{
											?>
											<span class="success">(Can be admitted)</span>
											<?php
										}
										else
										{
											?>
											<span class="error">(Can't be admitted)</span>
											<?php 
										}
									
									?>									
							 	</p>

							</div>

							<div class="col-xs-6">
								<input type="hidden" name="line_item[<?php echo $count; ?>]" value="<?php echo $line_item_count; ?>" /> 
								<input type="hidden" id="" name="ticket_admitted[<?php echo $count; ?>]" value="0">
								
								<?php 
								
								// Flag which tickets within an order can be auto submitted.
								if($canAdmit && $is_scanned) {?>
									<input type="hidden" id="" name="ticket_canadmit[<?php echo $count; ?>]" value="1">
																
								<?php } else {?>									
									<input type="hidden" id="" name="ticket_canadmit[<?php echo $count; ?>]" value="0">
								<?php }
								
								// Set the admitted flag against the tickets on the order.
								if($admitted){ ?>
									<input type="checkbox" name="ticket_admitted[<?php echo $count; ?>]" value="1" checked="checked" data-autoadmit="false">								
								<?php } else if($canAdmit && $is_scanned) {
									
										
									?>
								
									<input type="checkbox" name="ticket_admitted[<?php echo $count; ?>]" value="1" checked="checked" data-autoadmit="true">
								<?php } else{ ?>
									<input type="checkbox" name="ticket_admitted[<?php echo $count; ?>]" value="1" data-autoadmit="false">
								<?php } ?>
							</div>
						</div>
						
					<?php $count++; ?>

			    	<?php else: // Show all products (Adult, Child etc.) ?>

				    	<p><?php echo $lineItem['name']; ?> x<?php echo $lineItem['qty']; ?></p>

					<?php endif; ?>

						<input type="hidden" name="revised_searchterm" value="#<?php echo $order_id;?>" />
						
					</form>

				<?php 

				endforeach; ?>

				</div>
			
			</div>
		
			<div class="notes">
				<small>Notes:</small>
				<textarea name="notes"><?php echo get_field('notes', $order_id);?></textarea>
			</div>
			<hr>
			<input type="submit" class="btn-green" name="SAVE" value="SAVE" />		
			<button class="btn-brown"><a href="<?php the_permalink(82); ?>">CANCEL</a></button>	
		</div>
		</form>
		<?php
		}
		}
	/* 
		Extend the early entry check 
			
	*/
	function earlyEntryCheck($ticketDate, $order_date, $ticketEarliestAvail)
	{
		$message="";
		 // echo $ticketDate;
		 // echo $order_date;
		 // echo $ticketEarliestAvail;

		if(strtotime($ticketDate->format('Y-m-d')) > strtotime($order_date->add(new DateInterval('PT72H'))->format('Y-m-d')))
		{
			$message = '<p class="small">This ticket was bought in advance, the earliest entry is: '.$ticketEarliestAvail->format('d/m/y H:i:s')."</p>";
		}
									 	
		return $message;
	}

	/*
		Get the order back out using the order id
		use the index values that we have submitted in line_item to only update the line item at this index 
		with the value that we have got for the admitted value.
		Set the admitted flag on the ticket

		Order Id is an integer
		line item is an array - count of the item
		ticket admitted is an array of the values ticked on the form ?

	*/
	function updateAdmitted($order_id, $line_item, $ticket_admitted)
	{

		$order = new WC_Order($order_id); // get the order
		$count = 0;
		$lineItemId = 0;
		
		if(is_array($ticket_admitted))
		{
			// Loop through all products on the order	
		    foreach ($order->get_items() as $key => $lineItem)
		    {
		    	
		    	// TICKET product
				if($lineItem['product_id'] == 124 || $lineItem['product_id'] == 131)
				{

					$line_item_count = $count;
		
					if(count($ticket_admitted) >= $line_item_count+1)
					{
						wc_update_order_item_meta($key, 'Admitted', $ticket_admitted[$count]);
					}

					$count++;
				}

			}
		}
	}
	
	/* Starts with / ends with */
	function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}

	function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}


?>

<script type="text/javascript">

var autoAdmit = true;

jQuery(document).ready(function() {
	if(autoAdmit == true)
	{
		ticketChecks();
	}
});

function ticketChecks()
{
	// Count the number of orders on the page ... only should be submitting 1 order.
   if($(".ticket-panel").length == 1)
   {
	   var isscanned = $(".ticket-panel").data("isscanned");
	   var orderid = $(".ticket-panel").data("orderid");
	   
	   if(isscanned == 1)
	   {
		  // Now check to see whether or not this order contains tickets to be admitted.
		  
		  var canAdmitAny = false;
		  
			$('input[name^="ticket_canadmit"]').each(function() 
			{
				if($(this).val() == "1")
				{
					canAdmitAny = true;
					return false;
				}
			});
			
			if(canAdmitAny)
			{
				//console.log("Tickets here are valid - lets look to submit the form.")
				// Lets post the form.
				$("#"+orderid).submit();
			}
			else
			{
				//console.log("Tickets not valid lets stop");
				
				ticketError();
			}
	   }
   }
   /*
   else
   {
	   //console.log("ticket error");
	   ticketError();
   }*/
}

 
function ticketError()
{
	var assetsPath ="/wp-content/themes/bealepark/assets/"
	
	<!-- Setup error audio -->
	var audio = new Audio(assetsPath+"error.mp3");
	audio.play();
}
</script>