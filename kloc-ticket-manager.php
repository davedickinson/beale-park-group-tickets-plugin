<?php
/**
 * Plugin Name: KLOC Ticket Manager
 * Plugin URI: 
 * Description: Plugin for managing group tickets
 * Author: KLOC
 * Author URI: http://www.kloc.co.uk
 * Version: 0.1
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*-------------------------------------------
	Ticket Helper
--------------------------------------------*/
require_once( 'class-wc-ticket-helper.php');
require_once( 'class-wc-ticket-helper-settings.php');
require_once( 'class-wc-ticket-helper-extensions.php');

$ticket_helper = new WC_TicketHelper();

?>
